class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def shift_coordinates(self, x_shift=None, y_shift=None):
        if x_shift:
            self.x += x_shift

        if y_shift:
            self.y += y_shift

    def calculate_shift_coordinates(self, x_shift=0, y_shift=0):
        if x_shift:
            x = self.x + x_shift
        if y_shift:
            y = self.y + y_shift

        return Point(x, y)

    def __str__(self):
        return "Point(x={}; y={})".format(self.x, self.y)

    def __eq__(self, second_point):
        if self.x == second_point.x and self.y == second_point.y:
            return True
        else:
            return False

    def __contains__(self, points_list):
        for point in points_list:
            if self == point:
                return True
        return False
