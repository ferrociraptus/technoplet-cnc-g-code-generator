from os import mkdir
from typing import List
from PyQt5.QtCore import QStandardPaths
from First_portal_API_class import First_portal_API
from Second_portal_API_class import Second_portal_API
from line_profile_class import Line_profile
from knot_class import Knot
import pickle
from signals_emitter import Signals_emitter


class Technoplet_API:
    def __init__(self, list_of_knots: List[List[Knot]]):
        self.knots = list_of_knots

        self.unpack_parameters()

        self.g_code = self.start_g_code.copy()

        self.first_portal = First_portal_API(self.g_code, self.first_portal_max_x_pos, self.first_portal_max_y_pos,
                                             None,
                                             self.first_portal_x_offset, self.first_portal_y_offset,
                                             None, self.first_portal_standard_fedrate)

        self.first_portal.tension_mechanism_init(self.angle_for_tensioned_to_left,
                                                 self.angle_for_tensioned_to_right,
                                                 self.angle_for_homing_tensioned_mechanism)

        self.second_portal = Second_portal_API(self.g_code, self.second_portal_max_coil_mover_pos,
                                               self.second_portal_max_slider_pos,
                                               self.second_portal_max_coil_warehouse_pos,
                                               self.second_portal_coil_mover_offset, self.second_portal_slider_offset
                                               , None, self.second_portal_standard_fedrate)

        self.second_portal.coil_warehouse_init(self.coil_warehouse_up_pos, self.coil_warehouse_down_pos)
        self.signals_emitter = Signals_emitter()

    def add_new_profile(self, profile_name: str, line_diameter: float, knot_len: float, knot_width: float,
                        need_len_for_one_knot: float):
        self.line_profiles.append(Line_profile(profile_name, line_diameter, knot_len, knot_width,
                                               need_len_for_one_knot))

    def __set_default_frames_parameters(self):
        self.first_portal_max_x_pos = 200  # mm
        self.first_portal_max_y_pos = 300  # mm
        self.first_portal_x_offset = 0  # mm
        self.first_portal_y_offset = 0  # mm
        self.first_portal_standard_fedrate = 500  # mm/min

        self.second_portal_max_coil_mover_pos = 200  # mm
        self.second_portal_max_slider_pos = 200  # mm
        self.second_portal_max_coil_warehouse_pos = 50  # mm
        self.second_portal_coil_mover_offset = 0  # mm
        self.second_portal_slider_offset = 0  # mm
        self.second_portal_standard_fedrate = 500  # mm/min

        self.max_line_count = 12

    def __set_default_g_code_calculating_parameters(self):
        self.coil_mover_position_for_change_coils = 10  # mm

        self.coil_seats_distance_x = 5  # mm
        self.coil_seats_distance_y = 10  # mm

        self.angle_for_tensioned_to_left = 0
        self.angle_for_tensioned_to_right = 180
        self.angle_for_homing_tensioned_mechanism = 90

        self.second_portal_distance_from_coil_mover_to_line = 10  # mm
        self.second_portal_distance_from_slider_to_line = 10  # mm

        self.coil_warehouse_up_pos = 40  # mm
        self.coil_warehouse_down_pos = 0  # mm

        self.first_coil_warehouse_seat_x_pos = 150  # mm
        self.first_coil_warehouse_seat_y_pos = 200  # mm

        self.distance_from_lines_in_slider_work_area = 10  # mm
        self.distance_from_lines_in_coil_mover_work_area = 12  # mm
        self.knot_tightening_distance = 50  # mm
        self.first_portal_y_position_to_coil_mover = 120  # mm

        self.lines_shift_length = 30  # mm

        self.line_counts = 13
        self.line_profiles = []
        self.selected_line_profile = 0

    def __set_default_calculating_modes_parameters(self):
        self.extreme_coil_seats_mode = False
        self.comments_in_final_g_code = True
        self.start_g_code = ['G28']
        self.length_of_fabric = 100  # mm

    def save_all_parameters(self):
        self.save_frames_parameters()
        self.save_g_code_calculating_parameters()
        self.save_calculating_modes_options()

    def save_frames_parameters(self):
        self.save_config('frames_parameters.pickle', self.__convert_frames_parameters_to_list())

    def save_g_code_calculating_parameters(self):
        self.save_config('g_code_calculating_parameters.pickle',
                         self.__convert_g_code_calculating_parameters_to_list())

    def save_calculating_modes_options(self):
        self.save_config('modes_parameters_and_options.pickle', self.__convert_calculating_modes_options_to_list())

    def __convert_frames_parameters_to_list(self):
        return [self.first_portal_max_x_pos,
                self.first_portal_max_y_pos,
                self.first_portal_x_offset,
                self.first_portal_y_offset,
                self.first_portal_standard_fedrate,
                self.second_portal_max_coil_mover_pos,
                self.second_portal_max_slider_pos,
                self.second_portal_max_coil_warehouse_pos,
                self.second_portal_coil_mover_offset,
                self.second_portal_slider_offset,
                self.second_portal_standard_fedrate,
                self.max_line_count]

    def __convert_g_code_calculating_parameters_to_list(self):
        return [self.coil_mover_position_for_change_coils,
                self.coil_seats_distance_x,
                self.coil_seats_distance_y,
                self.angle_for_tensioned_to_left,
                self.angle_for_tensioned_to_right,
                self.angle_for_homing_tensioned_mechanism,
                self.second_portal_distance_from_coil_mover_to_line,
                self.second_portal_distance_from_slider_to_line,
                self.coil_warehouse_up_pos,
                self.coil_warehouse_down_pos,
                self.first_coil_warehouse_seat_x_pos,
                self.first_coil_warehouse_seat_y_pos,
                self.distance_from_lines_in_slider_work_area,
                self.distance_from_lines_in_coil_mover_work_area,
                self.knot_tightening_distance,
                self.first_portal_y_position_to_coil_mover,
                self.lines_shift_length,
                self.line_counts,
                self.line_profiles,
                self.selected_line_profile,
                ]

    def __convert_calculating_modes_options_to_list(self):
        return [self.extreme_coil_seats_mode,
                self.comments_in_final_g_code,
                self.start_g_code,
                self.length_of_fabric]

    def unpack_parameters(self):
        self.unpack_frames_parameters()
        self.unpack_g_code_calculating_parameters()
        self.unpack_calculating_modes_options()

    def unpack_frames_parameters(self):
        path = QStandardPaths.writableLocation(QStandardPaths.ConfigLocation)
        application_name = 'Technoplet_g_code_generator'
        try:
            with open(path + '/' + application_name + '/frames_parameters.pickle', 'rb') as file:
                parameters = pickle.load(file)
                self.first_portal_max_x_pos, \
                self.first_portal_max_y_pos, \
                self.first_portal_x_offset, \
                self.first_portal_y_offset, \
                self.first_portal_standard_fedrate, \
                self.second_portal_max_coil_mover_pos, \
                self.second_portal_max_slider_pos, \
                self.second_portal_max_coil_warehouse_pos, \
                self.second_portal_coil_mover_offset, \
                self.second_portal_slider_offset, \
                self.second_portal_standard_fedrate, \
                self.max_line_count = parameters
        except:
            self.__set_default_frames_parameters()

    def unpack_g_code_calculating_parameters(self):
        path = QStandardPaths.writableLocation(QStandardPaths.ConfigLocation)
        application_name = 'Technoplet_g_code_generator'
        try:
            with open(path + '/' + application_name + '/g_code_calculating_parameters.pickle', 'rb') as file:
                parameters = pickle.load(file)
                self.coil_mover_position_for_change_coils, \
                self.coil_seats_distance_x, \
                self.coil_seats_distance_y, \
                self.angle_for_tensioned_to_left, \
                self.angle_for_tensioned_to_right, \
                self.angle_for_homing_tensioned_mechanism, \
                self.second_portal_distance_from_coil_mover_to_line, \
                self.second_portal_distance_from_slider_to_line, \
                self.coil_warehouse_up_pos, \
                self.coil_warehouse_down_pos, \
                self.first_coil_warehouse_seat_x_pos, \
                self.first_coil_warehouse_seat_y_pos, \
                self.distance_from_lines_in_slider_work_area, \
                self.distance_from_lines_in_coil_mover_work_area, \
                self.knot_tightening_distance, \
                self.first_portal_y_position_to_coil_mover, \
                self.lines_shift_length, \
                self.line_counts, \
                self.line_profiles, \
                self.selected_line_profile = parameters
        except:
            self.__set_default_g_code_calculating_parameters()

    def unpack_calculating_modes_options(self):
        path = QStandardPaths.writableLocation(QStandardPaths.ConfigLocation)
        application_name = 'Technoplet_g_code_generator'
        try:
            with open(path + '/' + application_name + '/modes_parameters_and_options.pickle', 'rb') as file:
                parameters = pickle.load(file)
                self.extreme_coil_seats_mode, \
                self.comments_in_final_g_code, \
                self.start_g_code, \
                self.length_of_fabric = parameters
        except:
            self.__set_default_calculating_modes_parameters()

    def update_parameters(self):
        self.first_portal.max_x_position = self.first_portal_max_x_pos
        self.first_portal.max_y_position = self.first_portal_max_y_pos
        self.first_portal.x_offset = self.first_portal_x_offset
        self.first_portal.y_offset = self.first_portal_y_offset
        self.first_portal.standard_fedrate = self.first_portal_standard_fedrate

        self.first_portal.tension_left = self.angle_for_tensioned_to_left
        self.first_portal.tension_right = self.angle_for_tensioned_to_right
        self.first_portal.tension_home = self.angle_for_homing_tensioned_mechanism

        self.second_portal.max_x_position = self.second_portal_max_coil_mover_pos
        self.second_portal.max_y_position = self.second_portal_max_slider_pos
        self.second_portal.max_z_position_offset = self.second_portal_max_coil_warehouse_pos
        self.second_portal.x_offset = self.second_portal_coil_mover_offset
        self.second_portal.y_offset = self.second_portal_slider_offset
        self.second_portal.standard_fedrate = self.second_portal_standard_fedrate

        self.second_portal.warehouse_up = self.coil_warehouse_up_pos
        self.second_portal.warehouse_down = self.coil_warehouse_down_pos

    def take_coil_from_numbered_seat(self, coil_seat_number: int):
        # Read about in README file.
        if coil_seat_number % 2 == 0:
            x = self.first_coil_warehouse_seat_x_pos + coil_seat_number * self.coil_seats_distance_x
            y = self.first_coil_warehouse_seat_y_pos
        else:
            x = self.first_coil_warehouse_seat_x_pos + coil_seat_number * self.coil_seats_distance_x
            y = self.first_coil_warehouse_seat_y_pos + self.coil_seats_distance_y

        self.first_portal.move_stock_to_position(x, y)
        self.first_portal.take_coil()

    def put_coil_to_numbered_seat(self, coil_seat_number: int):
        if coil_seat_number % 2 == 0:
            x = self.first_coil_warehouse_seat_x_pos + coil_seat_number * self.coil_seats_distance_x
            y = self.first_coil_warehouse_seat_y_pos
        else:
            x = self.first_coil_warehouse_seat_x_pos + coil_seat_number * self.coil_seats_distance_x
            y = self.first_coil_warehouse_seat_y_pos + self.coil_seats_distance_y

        self.first_portal.move_stock_to_position(x, y)
        self.first_portal.put_coil()

    def change_coils_after_knot_realise(self, captured_coil_seat_number: int, second_coil_seat_number: int):
        # After making knot coil located in the stock.
        # And we can change coils immediately without backing coil into place
        self.second_portal.move_coil_mover(self.coil_mover_position_for_change_coils)

        self.first_portal.move_stock_to_position(self.coil_mover_position_for_change_coils,
                                                 self.first_portal_y_position_to_coil_mover)

        self.first_portal.put_coil()
        self.take_coil_from_numbered_seat(second_coil_seat_number)
        self.put_coil_to_numbered_seat(captured_coil_seat_number)

        self.first_portal.move_stock_to_position(self.coil_mover_position_for_change_coils,
                                                 self.first_portal_y_position_to_coil_mover)

        self.first_portal.take_coil()
        self.put_coil_to_numbered_seat(second_coil_seat_number)

    def change_coils(self, first_coil_seat_number, second_coil_seat_number):
        self.second_portal.move_coil_mover(self.coil_mover_position_for_change_coils)

        self.take_coil_from_numbered_seat(first_coil_seat_number)

        self.first_portal.move_stock_to_position(self.coil_mover_position_for_change_coils,
                                                 self.first_portal_y_position_to_coil_mover)

        self.first_portal.put_coil()
        self.take_coil_from_numbered_seat(second_coil_seat_number)
        self.put_coil_to_numbered_seat(first_coil_seat_number)

        self.first_portal.move_stock_to_position(self.coil_mover_position_for_change_coils,
                                                 self.first_portal_y_position_to_coil_mover)

        self.first_portal.take_coil()
        self.put_coil_to_numbered_seat(second_coil_seat_number)

    def shift_lines_left(self, line_number: int):
        if line_number > 2 or not self.extreme_coil_seats_mode:
            self.second_portal.slider_down()
            position = self.second_portal_distance_from_slider_to_line + \
                       self.distance_from_lines_in_slider_work_area * (line_number - 0.5)
            self.second_portal.move_slider(position)
            self.second_portal.slider_up()
            self.second_portal.move_slider(position - self.lines_shift_length)
        else:
            self.shift_lines_right(line_number - 1)

    def shift_lines_right(self, line_number):
        if line_number < self.line_counts or not self.extreme_coil_seats_mode:
            self.second_portal.slider_down()
            position = self.second_portal_distance_from_slider_to_line \
                       + self.distance_from_lines_in_slider_work_area * (line_number + 0.5)
            self.second_portal.move_slider(position)
            self.second_portal.slider_up()
            self.second_portal.move_slider(position + self.lines_shift_length)
        else:
            self.shift_lines_left(line_number + 1)

    def make_knot_left(self, number_of_base_line: int):
        work_line = number_of_base_line - 1

        if self.line_counts > number_of_base_line >= 0 and self.line_counts > work_line >= 0:
            self.take_coil_from_numbered_seat(work_line)
            self.shift_lines_left(number_of_base_line)
            x_position = self.second_portal_distance_from_coil_mover_to_line + \
                         self.distance_from_lines_in_coil_mover_work_area * (number_of_base_line - 0.5) - \
                         self.lines_shift_length * 0.5
            self.first_portal.move_stock_to_position(x_position, self.first_portal_y_position_to_coil_mover)
            self.first_portal.tension_to_right()
            self.second_portal.move_coil_mover(x_position)
            self.first_portal.put_coil()
            self.shift_lines_right(number_of_base_line)
            x_position = self.second_portal_distance_from_coil_mover_to_line + \
                         self.distance_from_lines_in_coil_mover_work_area * (number_of_base_line + 0.5) + \
                         self.lines_shift_length * 0.5
            self.first_portal.move_stock_to_position(x_position)
            self.second_portal.move_coil_mover(x_position)
            self.first_portal.take_coil()
            self.first_portal.tension_to_home()
            self.first_portal.move_stock_to_position(None, self.first_portal_y_position_to_coil_mover
                                                     - self.knot_tightening_distance)
            self.first_portal.move_stock_to_position(None, self.first_portal_y_position_to_coil_mover)
            self.first_portal.wind(- float(self.line_profiles[self.selected_line_profile].need_len_for_one_knot) / 2.0)
        else:
            raise ValueError('Incorrect_frame_input')

    def make_knot_right(self, number_of_base_line: int):
        work_line = number_of_base_line + 1

        if self.line_counts > number_of_base_line >= 0 and self.line_counts > work_line >= 0:
            self.take_coil_from_numbered_seat(work_line)
            self.shift_lines_right(number_of_base_line)
            x_position = self.second_portal_distance_from_coil_mover_to_line + \
                         self.distance_from_lines_in_coil_mover_work_area * (number_of_base_line + 0.5) + \
                         self.lines_shift_length * 0.5
            self.first_portal.move_stock_to_position(x_position, self.first_portal_y_position_to_coil_mover)
            self.first_portal.tension_to_left()
            self.second_portal.move_coil_mover(x_position)
            self.first_portal.put_coil()
            self.shift_lines_left(number_of_base_line)
            x_position = self.second_portal_distance_from_coil_mover_to_line + \
                         self.distance_from_lines_in_coil_mover_work_area * (number_of_base_line - 0.5) - \
                         self.lines_shift_length * 0.5
            self.first_portal.move_stock_to_position(x_position)
            self.second_portal.move_coil_mover(x_position)
            self.first_portal.take_coil()
            self.first_portal.tension_to_home()
            self.first_portal.move_stock_to_position(None, self.first_portal_y_position_to_coil_mover
                                                     - self.knot_tightening_distance)
            self.first_portal.move_stock_to_position(None, self.first_portal_y_position_to_coil_mover)
            self.first_portal.wind(- float(self.line_profiles[self.selected_line_profile].need_len_for_one_knot) / 2.0)
        else:
            raise ValueError('Incorrect_frame_input')

    def generate_g_code(self):
        self.clean_g_code()
        if len(self.line_profiles) == 0:
            self.signals_emitter.emit_no_profile()
        else:
            need_rows = int(self.line_profiles[self.selected_line_profile].need_rows_to_len(self.length_of_fabric))

            rows_counter = 0
            break_while = False
            while rows_counter < need_rows:
                for row in range(0, len(self.knots)):
                    if self.comments_in_final_g_code:
                        self.g_code.append('# Row: {}'.format(rows_counter))
                    if rows_counter == need_rows:
                        break_while = True
                        break
                    for knot in self.knots[row]:
                        if knot is not None:
                            knot_type = knot.image_rotate.literal()

                            if knot_type[0] == 'L':
                                base_line = knot.position.y + 1
                                self.make_knot_left(base_line)
                            elif knot_type[0] == 'R':
                                base_line = knot.position.y
                                self.make_knot_right(base_line)

                            if knot_type[1] == 'L':
                                self.make_knot_left(base_line)
                                work_line = base_line - 1
                                self.change_coils_after_knot_realise(work_line, base_line)
                            elif knot_type[1] == 'R':
                                self.make_knot_right(base_line)
                                work_line = base_line + 1
                                self.change_coils_after_knot_realise(work_line, base_line)

                    rows_counter += 1
                    self.second_portal.wind_fabric(self.line_profiles[self.selected_line_profile].knot_len)
                if break_while:
                    break

    def clean_g_code(self):
        if len(self.line_profiles) == 0:
            self.signals_emitter.emit_no_profile()
        else:
            self.g_code.clear()
            if self.comments_in_final_g_code:
                need_rows = int(self.line_profiles[self.selected_line_profile].need_rows_to_len(self.length_of_fabric))
                self.g_code.append('# Generate by Technoplet v1.0.0')
                self.g_code.append('#')
                self.g_code.append('# Fabric:')
                self.g_code.append('#   Rows: {}'.format(need_rows))
                self.g_code.append('#   Lines: {}'.format(self.line_counts))
                self.g_code.append('#')
                self.g_code.append('# Start G-code:')

            for line in self.start_g_code:
                self.g_code.append(line)

            self.g_code.append('#')

    def load_g_code_from_file(self, path: str):
        try:
            with open(path, 'r') as text:
                text_parse = text.readlines()
            self.g_code.clear()
            for line in text_parse:
                self.g_code.append(line)
        except:
            pass

    @staticmethod
    def save_config(file_name, data):
        path = QStandardPaths.writableLocation(QStandardPaths.ConfigLocation)
        application_name = 'Technoplet_g_code_generator'
        try:
            with open(path + '/' + application_name + '/' + file_name, 'wb') as file:
                pickle.dump(data, file)
        except Exception:
            mkdir(path + '/' + application_name, 0o700)
            with open(path + '/' + application_name + '/' + file_name, 'wb') as file:
                pickle.dump(data, file)