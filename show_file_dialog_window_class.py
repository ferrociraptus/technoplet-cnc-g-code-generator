from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QMainWindow, QGridLayout, QPushButton, QTextEdit, QWidget


class Show_file_Dialog(QMainWindow):

    def __init__(self):
        self.text_file = ''

        super().__init__()
        self.setWindowTitle('Readme')
        self.resize(700, 900)

        self.grid_widget = QWidget(self)
        self.grid = QGridLayout(self.grid_widget)

        self.text_edit = QTextEdit()
        self.text_edit.setReadOnly(True)
        self.text_edit.setFont(QFont('Courier New', 10, QFont.Bold))

        self.exit_button = QPushButton()
        self.exit_button.setText('Cancel')
        self.exit_button.clicked.connect(self.close)

        self.setCentralWidget(self.grid_widget)

        self.grid.addWidget(self.text_edit, 0, 0)
        self.grid.addWidget(self.exit_button, 1, 0)

    def setFile(self, text_file_path: str):
        self.text_file = text_file_path
        try:
            with open(self.text_file, 'r') as text:
                self.text_edit.setText(text.read())
        except:
            raise ValueError('Incorrect text file path.')
