from resource_path import resource_path


class ImageRotate:
    paints = [resource_path('images/Right_Right.svg'),
              resource_path('images/Right_Left.svg'),
              resource_path('images/Left_Left.svg'),
              resource_path('images/Left_Right.svg')]

    paints_literals = ['RR', 'RL', 'LL', 'LR']

    def __init__(self, knot):
        self.counter = 3
        self.knot = knot
        self.next_image(0)

    def next_image(self, shift=1) -> str:
        if type(shift) != int:
            raise ValueError('Incorrect shift input.')

        self.counter += shift

        if self.counter >= len(self.paints):
            self.counter = 0

        self.knot.image = self.image()
        self.knot.knot_type = self.literal()

        return self.paints[self.counter]

    def literal(self) -> str:
        return self.paints_literals[self.counter]

    def image(self, count=None) -> str:
        if count is not None:
            if type(count) == int:
                if count <= -len(self.paints) or count > len(self.paints) - 1:
                    return self.paints[count]
                else:
                    raise ValueError("Address isn't correct or image isn't standard knot icon.")
            else:
                raise ValueError('Incorrect index input.')
        else:
            return self.paints[self.counter]

    def return_to_default(self):
        self.counter = 3
        self.knot.image = self.image()
        self.knot.knot_type = self.literal()

