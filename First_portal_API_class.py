from G_code_class import G_code_generator


class First_portal_API(G_code_generator):
    # B<index> (G-command)
    # Custom g-code for distributing g-commands to different portals
    # B - Block
    # <index> - index of portal
    def tension_mechanism_init(self, left_pos_angle: int, right_pos_angle: int, home_pos_angle: int):
        self.tension_left = left_pos_angle
        self.tension_right = right_pos_angle
        self.tension_home = home_pos_angle

    def move_stock_to_position(self, x: float = None, y: float = None, fedrate: float = None):
        self.move_to_position('P0', x, y, None, None, fedrate)

    def auto_home(self):
        self.auto_home_axis('P0', 'XY')
        self.restart_servos_or_solenoids('P0', None, None)

    def stock_up(self):
        self.solenoid_move('P0', 0, 0)

    def stock_down(self):
        self.solenoid_move('P0', 0, 1)

    def wind(self, length: float):
        self.move_to_position('P0', None, None, None, length)

    def tension_to_left(self):
        self.tension_to_home()
        self.solenoid_move('P0', 2, 1)
        self.servo_move('P0', 0, self.tension_left)

    def tension_to_right(self):
        self.tension_to_home()
        self.solenoid_move('P0', 2, 1)
        self.servo_move('P0', 0, self.tension_right)

    def tension_to_home(self):
        self.servo_move('P0', 0, self.tension_home)
        self.solenoid_move('P0', 2, 0)

    def take_coil(self):
        self.solenoid_move('P0', 0, 1)  # stock down
        self.solenoid_move('P0', 1, 1)  # electromagnet turn on
        self.solenoid_move('P0', 0, 0)  # stock up

    def put_coil(self):
        self.solenoid_move('P0', 0, 1)  # stock down
        self.solenoid_move('P0', 1, 0)  # electromagnet turn off
        self.solenoid_move('P0', 0, 0)  # stock up
