from PyQt5 import QtWidgets
from point_class import Point

from knot_class import Knot
from line_class import Line


class Fabric():                                                             # fabric    column     row      line
    def __init__(self, grid, rows, lines):                                  # ||||||      |       ||||||    |
        self.grid = grid                                                    # ||\/||      |                  \
        self.rows = rows                                                    # ||/\||      |                   |
        self.columns = lines - 1                                            # ||||||      |                   |
        self.lines = []  # everything line list have all knot on one line   # ||||||      |                   |
        self.knots = []  # list with all knots
        self.widget_grid_layout_for_lines = QtWidgets.QWidget(grid)
        self.grid_layout_for_lines = QtWidgets.QGridLayout(self.widget_grid_layout_for_lines)
        self.grid_layout_for_organisation = QtWidgets.QFormLayout(grid)

        self.widget_fabric_sceme_grid_layout = QtWidgets.QWidget(grid)
        self.fabric_scheme_grid_layout = QtWidgets.QGridLayout(self.widget_fabric_sceme_grid_layout)

        self.fabric_scheme_grid_layout.setHorizontalSpacing(5)
        self.fabric_scheme_grid_layout.setVerticalSpacing(15)
        self.fabric_scheme_grid_layout.activate()

        self.grid_layout_for_lines.setHorizontalSpacing(5)
        self.grid_layout_for_lines.setVerticalSpacing(15)
        self.grid_layout_for_lines.activate()

        self.grid_layout_for_organisation.addRow(self.widget_grid_layout_for_lines)
        self.grid_layout_for_organisation.addRow(self.widget_fabric_sceme_grid_layout)

        self.prototype_part_of_fabric()

    def prototype_part_of_fabric(self):
        for line in range(0, self.columns + 1):
            self.lines.append(Line(self.grid, self.grid_layout_for_lines, 0, line))

        for row in range(0, self.rows):
            buff_row = []
            if row % 2 == 0:
                for line in range(0, self.columns, 2):
                    buff = Knot(self.grid)
                    buff.knot_init(row, line, self)
                    self.fabric_scheme_grid_layout.addWidget(buff, row, line)
                    buff_row.append(buff)
                    buff_row.append(None)
            else:
                for line in range(1, self.columns, 2):
                    buff = Knot(self.grid)
                    buff.knot_init(row, line, self)
                    self.fabric_scheme_grid_layout.addWidget(buff, row, line)
                    buff_row.append(None)
                    buff_row.append(buff)
            self.knots.append(buff_row)
        self.grid_update()

    def del_lines(self, lines=1):
        for line in range(0, lines):
            self.__remove_line(self.lines[len(self.lines) - 1])
            if len(self.knots[0]) == len(self.knots[1]):
                for row in range(1, self.rows, 2):
                    if self.knots[row][len(self.knots[row]) - 1] is not None:
                        self.__remove_knot(self.knots[row][len(self.knots[row]) - 1])
                    del self.knots[row][len(self.knots[row]) - 2:]
            else:
                for row in range(0, self.rows, 2):
                    if self.knots[row][len(self.knots[row]) - 2] is not None:
                        self.__remove_knot(self.knots[row][len(self.knots[row]) - 2])
                    del self.knots[row][len(self.knots[row]) - 2:]

            self.lines.pop(len(self.lines) - 1)

        self.columns -= lines
        self.grid_update()

    def add_lines(self, lines=1):
        for count in range(0, lines):
            self.lines.append(Line(self.grid, self.grid_layout_for_lines, 0, len(self.lines) + count))
            line = self.columns + count
            if len(self.knots[0]) == len(self.knots[1]):
                for row in range(0, self.rows, 2):
                    buff = Knot(self.grid)
                    buff.knot_init(row, line, self)
                    self.fabric_scheme_grid_layout.addWidget(buff, row, line)
                    self.knots[row].append(buff)
                    self.knots[row].append(None)

            else:
                for row in range(1, self.rows, 2):
                    buff = Knot(self.grid)
                    buff.knot_init(row, line, self)
                    self.fabric_scheme_grid_layout.addWidget(buff, row, line)
                    self.knots[row].append(None)
                    self.knots[row].append(buff)

        self.columns += lines
        self.grid_update()

    def add_rows(self, rows=1):
        for i in range(0, rows):
            buff = []
            if (self.rows + i) % 2 == 0:
                for line in range(0, self.columns, 2):
                    knot_object = Knot(self.grid)
                    knot_object.knot_init(self.rows + i, line, self)
                    self.fabric_scheme_grid_layout.addWidget(knot_object, self.rows + i, line)
                    buff.append(knot_object)
                    buff.append(None)
            else:
                for line in range(1, self.columns, 2):
                    knot_object = Knot(self.grid)
                    knot_object.knot_init(self.rows + i, line, self)
                    self.fabric_scheme_grid_layout.addWidget(knot_object, self.rows + i, line)
                    buff.append(None)
                    buff.append(knot_object)

            self.knots.append(buff)
        self.rows += rows
        self.grid_update()

    def del_rows(self, rows=1):
        for i in range(0, rows):
            for knot in self.knots[len(self.knots) - 1]:
                if knot is not None:
                    self.__remove_knot(knot)
            del self.knots[len(self.knots) - 1]

        self.rows -= rows
        self.grid_update()

    # def grid_resize(self):
    #     self.grid.setGeometry(QtCore.QRect(20, 10, 300, 1000))

    def grid_update(self):
        self.calculate_lines()

    def __remove_knot(self, knot: Knot):
        ip = self.fabric_scheme_grid_layout.itemAtPosition(knot.position.x, knot.position.y)
        self.fabric_scheme_grid_layout.removeItem(ip)
        self.fabric_scheme_grid_layout.removeWidget(ip.widget())
        ip.widget().deleteLater()

    def __remove_line(self, line: Line):
        ip = self.grid_layout_for_lines.itemAtPosition(line.line_positions[0].x, line.line_positions[0].y)
        self.grid_layout_for_lines.removeItem(ip)
        self.grid_layout_for_lines.removeWidget(ip.widget())
        ip.widget().deleteLater()

    def set_random_colors_to_lines(self):
        for line in self.lines:
            line.set_random_color()

    def return_to_default(self):
        for line in self.lines:
            line.change_color('#ffffff')
            for knot in line.line_knots:
                knot.image_rotate.return_to_default()
                knot.update_image()
        self.calculate_lines()

    def calculate_lines_to_row(self, row: int):
        self.calculate_lines()
        shift = self.rows
        while len(self.lines[-1].line_positions) - 1 < row:
            for line in self.lines:
                for lIne in self.lines:
                    if Point(0, line.line_positions[-1].y) in lIne.line_positions:
                        index = self.lines.index(lIne)
                        break
                for row in range(0, self.rows):
                    if len(line.line_positions) - 1 < row:
                        line.add_line_position(self.lines[index].line_positions[row]
                                               .calculate_shift_coordinates(shift, 0))
            shift += self.rows

    def calculate_lines(self):
        for line in self.lines:
            line.clean()

            row = 0
            column = line.line_positions[0].y

            if line.line_positions[0].y % 2 == 0:
                side = 'L'
            else:
                side = 'R'
                column -= 1

            for position in line.line_positions:

                if column < 0:
                    if row < self.rows - 1:
                        line.add_line_position(row, column)
                    column += 1
                    row += 1
                    side = 'L'

                elif column > self.columns - 1:
                    if row < self.rows - 1:
                        line.add_line_position(row, column)
                    column -= 1
                    row += 1
                    side = 'R'

                if row < self.rows:

                    try:
                        if side == 'L':

                            if self.knots[row][column].knot_type[0] == 'L':
                                line.add_knot(self.knots[row][column])

                                if self.knots[row][column].knot_type[1] == 'L':
                                    column -= 1
                                    side = 'R'
                                elif self.knots[row][column].knot_type[1] == 'R':
                                    column += 1
                                    side = 'L'


                            elif self.knots[row][column].knot_type[0] == 'R':
                                line.add_line_position(row, column)

                                if self.knots[row][column].knot_type[1] == 'L':
                                    column += 1
                                    side = 'L'
                                elif self.knots[row][column].knot_type[1] == 'R':
                                    column -= 1
                                    side = 'R'

                        elif side == 'R':

                            if self.knots[row][column].knot_type[0] == 'L':
                                line.add_line_position(row, column)

                                if self.knots[row][column].knot_type[1] == 'L':
                                    column += 1
                                    side = 'L'
                                elif self.knots[row][column].knot_type[1] == 'R':
                                    column -= 1
                                    side = 'R'


                            elif self.knots[row][column].knot_type[0] == 'R':
                                line.add_knot(self.knots[row][column])

                                if self.knots[row][column].knot_type[1] == 'L':
                                    column -= 1
                                    side = 'R'
                                elif self.knots[row][column].knot_type[1] == 'R':
                                    column += 1
                                    side = 'L'
                        row += 1
                    except AttributeError:
                        print("""Debug:
                        Error: AttributeError(None on the position)
                        line number: {}
                        row: {}
                        column: {}""".format(self.lines.index(line), row, column))

                    except IndexError:
                        print("""Debug:
                        Error: IndexError(Index out of range)
                        line number: {}
                        row: {}
                        column: {}""".format(self.lines.index(line), row, column))
            line.update_knot_color()
