from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5.QtGui import QResizeEvent, QCloseEvent, QPalette
from interface_UI import Interface_Ui
import sys


class My_window(QMainWindow):
    def __init__(self):
        super(My_window, self).__init__()
        self.ui = Interface_Ui(self)
        self.ui.init_ui()

    def resizeEvent(self, a0: QResizeEvent) -> None:
        print(self.size())
        # self.ui.resize()
        super().resizeEvent(a0)

    def closeEvent(self, event: QCloseEvent) -> None:
        # super().closeEvent()
        reply = QMessageBox.question(self, 'Message',
            "Are you sure to quit?", QMessageBox.Yes |
            QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

        while self.ui.thread_finish:
            pass
        self.ui.close_all()


if __name__ == "__main__":
    app = QApplication([])
    application = My_window()
    application.show()
    sys.exit(app.exec())
