class Color:

    def __init__(self, red=None, green=None, blue=None):
        self.red = 0
        self.green = 0
        self.blue = 0
        self.__color_parser(red, green, blue)

    def change_color(self, red=None, green=None, blue=None):
        if type(red) == Color:
            self.red, self.green, self.blue = red.color_in_rgb()
            self._color_in_hex = red._color_in_hex
        else:
            self.__color_parser(red, green, blue)

    def color_in_hex(self, hex_literal: str) -> str:
        # Add hex literal in begin of color record in hex format
        return hex_literal + self._color_in_hex

    def color_in_rgb(self) -> list:
        rgb = [self.red, self.green, self.blue]
        return rgb

    def __color_parser(self, red, green, blue):
        if red is not None and green is None and blue is None:
            color_in_hex = str(red)
            if (color_in_hex[:2] == '0x' and len(color_in_hex) == 8) or (
                    color_in_hex[:1] == '#' and len(color_in_hex) == 7):
                if color_in_hex[:2] == '0x':
                    self._color_in_hex = color_in_hex[2:]
                elif color_in_hex[:1] == '#':
                    self._color_in_hex = color_in_hex[1:]
            else:
                raise ValueError("Record isn't correct or mistake in line.")
            self.__hex_to_rgb()

        else:
            self._color_in_hex = ''

            if type(red) == int:
                self._color_in_hex += self.symbols_count_standard(str(hex(red))[2:])
                self.red = self.red

            if type(green) == int:
                self._color_in_hex += self.symbols_count_standard(str(hex(green))[2:])
                self.green = green

            if type(blue) == int:
                self._color_in_hex += self.symbols_count_standard(str(hex(blue))[2:])
                self.blue = blue

    def __hex_to_rgb(self):
        self.red = int(self._color_in_hex[0:2], 16)
        self.green = int(self._color_in_hex[2:4], 16)
        self.blue = int(self._color_in_hex[4:6], 16)

    @staticmethod
    def symbols_count_standard(hex_format: str) -> str:
        if len(hex_format) < 2:
            return '0' * (2 - len(hex_format)) + hex_format
        elif len(hex_format) == 2:
            return hex_format
        elif len(hex_format) > 2:
            raise ValueError('Uncorrected input ot value not in range [0, 255]')