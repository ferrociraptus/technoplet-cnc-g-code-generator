from typing import List

from PyQt5 import QtGui
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QMainWindow, QListWidget, QGridLayout, QWidget, QLabel, QPushButton, QErrorMessage
from line_profile_class import Line_profile
from edit_line_profiles_dialog_window import Edit_line_profile_Dialog
from signals_emitter import Signals_emitter


class Show_lines_profiles_Dialog(QMainWindow):
    __names_font = QFont("Times New Roman", 16, QFont.Bold)
    __buttons_font = QFont("DejaVu Sans", 14, QFont.Bold)
    show_lines_profiles_dialog_close = pyqtSignal()

    def __init__(self, line_profiles: List[Line_profile]):
        super().__init__()
        self.setWindowTitle('Line profiles')
        self.setFixedSize(430, 250)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

        self.error_message = QErrorMessage()
        self.error_message = QErrorMessage()
        self.error_message.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.error_message.setWindowTitle('No line profiles')

        self.__signals_emitter = Signals_emitter()
        self.line_profiles = line_profiles
        self.edit_line_profile_dialog = Edit_line_profile_Dialog()
        self.edit_line_profile_dialog.edit_line_profile_dialog_close.connect(self.__edit_line_profile_dialog_close_handler)
        self.edit_line_profile_dialog.new_line_profile_was_created_signal.connect(
            self.__new_line_profile_was_created_signal_handler)

        self.window_grid_widget = QWidget()
        self.window_grid = QGridLayout(self.window_grid_widget)
        self.setCentralWidget(self.window_grid_widget)

        self.grid_for_show_list_profile_widget = QWidget()
        self.grid_for_show_list_profile_widget.setMaximumHeight(200)
        self.grid_for_show_list_profile = QGridLayout(self.grid_for_show_list_profile_widget)

        self.list_widget = QListWidget()
        self.__update_list_widget()
        self.list_widget.itemSelectionChanged.connect(self.__list_widget_handler)

        self.line_profile_name_label = QLabel()
        self.line_profile_name_label.setText('___')
        self.line_profile_name_label.setFont(Show_lines_profiles_Dialog.__names_font)

        self.line_diameter_label = QLabel()
        self.line_diameter_label.setText('Line diameter: _')

        self.need_len_for_one_knot_label = QLabel()
        self.need_len_for_one_knot_label.setText('Need line for knot: _')

        self.knot_len_label = QLabel()
        self.knot_len_label.setText('Knot length: _')

        self.knot_width_label = QLabel()
        self.knot_width_label.setText('Knot width: _')

        self.add_new_line_profile_button = QPushButton()
        self.add_new_line_profile_button.setFont(Show_lines_profiles_Dialog.__buttons_font)
        self.add_new_line_profile_button.setText('Add new')
        self.add_new_line_profile_button.clicked.connect(self.__add_new_line_profile_button_handler)

        self.edit_current_line_profile_button = QPushButton()
        self.edit_current_line_profile_button.setFont(Show_lines_profiles_Dialog.__buttons_font)
        self.edit_current_line_profile_button.setText('Edit')
        self.edit_current_line_profile_button.clicked.connect(self.__edit_current_line_profile_button_handler)
        self.edit_current_line_profile_button.setFixedWidth(100)

        self.remove_current_line_profile_button = QPushButton()
        self.remove_current_line_profile_button.setFont(Show_lines_profiles_Dialog.__buttons_font)
        self.remove_current_line_profile_button.setText('Delete')
        self.remove_current_line_profile_button.clicked.connect(self.__remove_current_line_profile_button_handler)
        self.remove_current_line_profile_button.setFixedWidth(100)

        self.grid_for_show_list_profile.addWidget(self.line_profile_name_label, 0, 0, 1, 2)
        self.grid_for_show_list_profile.addWidget(self.line_diameter_label, 1, 0, 1, 2)
        self.grid_for_show_list_profile.addWidget(self.need_len_for_one_knot_label, 2, 0, 1, 2)
        self.grid_for_show_list_profile.addWidget(self.knot_len_label, 3, 0, 1, 2)
        self.grid_for_show_list_profile.addWidget(self.knot_width_label, 4, 0, 1, 2)
        self.grid_for_show_list_profile.addWidget(self.edit_current_line_profile_button, 5, 0, 1, 1)
        self.grid_for_show_list_profile.addWidget(self.remove_current_line_profile_button, 5, 1, 1, 1)

        self.window_grid.addWidget(self.list_widget, 0, 0, 2, 1)
        self.window_grid.addWidget(self.grid_for_show_list_profile_widget, 0, 1, 1, 1)
        self.window_grid.addWidget(self.add_new_line_profile_button, 1, 1, 1, 1)

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.edit_line_profile_dialog.close()
        self.__signals_emitter.emit_signal(self.show_lines_profiles_dialog_close)
        self.__signals_emitter.emit_some_parameter_was_change()
        super().close()

    def __list_widget_handler(self):
        index = self.list_widget.currentIndex().row()
        if index >= 0:
            try:
                self.line_profile_name_label.setText('{}'.format(self.line_profiles[index].profile_name))
                self.line_diameter_label.setText('Line diameter: {}'.format(self.line_profiles[index].line_diameter))
                self.need_len_for_one_knot_label.setText('Need line for knot: {}'
                                                         .format(self.line_profiles[index].need_len_for_one_knot))
                self.knot_len_label.setText('Knot length: {}'.format(self.line_profiles[index].knot_len))
                self.knot_width_label.setText('Knot width: {}'.format(self.line_profiles[index].knot_width))
            except:
                self.line_profile_name_label.setText('___')
                self.line_diameter_label.setText('Line diameter: _')
                self.need_len_for_one_knot_label.setText('Need line for knot: _')
                self.knot_len_label.setText('Knot length: _')
                self.knot_width_label.setText('Knot width: _')

    def __add_new_line_profile_button_handler(self):
        self.setDisabled(True)
        self.edit_line_profile_dialog.add_line_profile()

    def __edit_current_line_profile_button_handler(self):
        index = self.list_widget.currentIndex().row()
        if index >= 0:
            self.setDisabled(True)
            self.edit_line_profile_dialog.rewrite_line_profile(self.line_profiles[index])
        else:
            self.error_message.showMessage("Set profile before editing.")

    def __remove_current_line_profile_button_handler(self):
        index = self.list_widget.currentIndex().row()
        if index >= 0:
            self.line_profiles.pop(index)
            self.__update_list_widget()
        else:
            self.error_message.showMessage("Set profile before removing.")

    def __update_list_widget(self):
        self.list_widget.clear()
        for line_profile in self.line_profiles:
            self.list_widget.addItem(str(line_profile.profile_name))

    def __new_line_profile_was_created_signal_handler(self, line_profile):
        self.line_profiles.append(line_profile)
        self.__update_list_widget()

    def __edit_line_profile_dialog_close_handler(self):
        self.setDisabled(False)