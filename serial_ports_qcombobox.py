from PyQt5.QtWidgets import QComboBox
import serial.tools.list_ports


class Serial_ports_QComboBox(QComboBox):
    __gray = "background-color: rgb(200, 200, 200);"

    def __init__(self, *args, **kwargs):
        super(QComboBox, self).__init__(*args, **kwargs)
        self.data = []
        self.fill_values()

    def showPopup(self):
        super().showPopup()
        self.fill_values()
        self.setStyleSheet(Serial_ports_QComboBox.__gray)

    def fill_values(self):
        self.clear()
        self.addItem("")
        self.data.append(d.device for d in serial.tools.list_ports.comports())
        for i in self.data:
            self.addItems(i)
        self.setCurrentIndex(1)
