This program write to generate specific G-code to Technoplet CNC machine.
This machine is two portal system for automatic create specific type of fabric.

In this project I use two controllers with GRBL firmware and third controller-parser with my firmware
 where I can realise or rewrite some custom G-code commands.
Because I need to generate G-code with standard G-code and with custom.


Custom G-codes:

    1) G-code prefix for distributing g-commands to different portals:
        P<index> (G-command)
           P - Portal
           <index> - index of portal
           
        If command without prefix command send to both portals.

    2) G-code for servo move
        S1 I<index> L<position>
        <index> - servo index to set
        <position> - position index(for example: angle)

    3) G-code for solenoid move
        S2 I<index> L<position>
        <index> - servo index to set
        <position> - position index 1 or 0(open or close)

    4) G-code for solenoid move
        S0 T<type index> I<index>
        <type_index> - 0(servo type index), 1(solenoid type index)
        <index> - solenoid or servo index to set
        if both parameters is NONE restart all solenoids and servos
        if only type index parameter restart all set devises


Supplemented G-code:

    1) G-code for moving
        G1 X<pos> Y<pos> Z<pos> E<pos> F<rate>
        E<pos> - the length of line to feed into coil.
        X<pos> - a coordinate on the X axis
        Y<pos> - a coordinate on the Y axis
        Z<pos> - coordinate on the Z axis
        <rate> - the maximum movement rate of the move between the start and end point.
        The feed rate set here applies to subsequent moves that omit this parameter.


First portal(Number: 0):

    First portal have plaiting head(first block) which can move on X and Y origin in the same time.

    Head have stock which can take coil by electromagnet.
    Stock can work only on Z origin and be in two positions(up/down).
    Stock controlled by motor which simulate solenoid.
    In the same time stock have controlled rotate(which set in moving G-command in E parameter).

    Also head have tensioned mechanism with one servo and solenoid.
    Servo have 3 positions 0, 90, 180 (may be deviations).
```  
     _____________
     |           |
 Y^  |           |
  |  |           |
  |  |           |
  |  |       ___ |
  |  |-------|_|-|
  |  |___________|
  |                X
  |---------------->


    ___
    |_| - head

    ----------- - guide rail
```

    Portal:
        All subsequent G-commands for head(first block) need to write with first portal prefix.

        Portal prefix: 'P0'
        Portal number: '0'


    Head:
        All G-code commands for moving with adding prefix.
        Example: 'P0 G0 X43.6 Y84.2'


    Stock:
        Can be controlled rotate by stepper motor. Don't have limited switch.
        For rotate need to use moving command with set E parameter(don't forget special prefix).
        Example: 'P0 G1 E100.3'

        Can be move on Z axis by solenoid into two position(up/down).
        Need to use custom G-code commands with special prefix.
        Example: 'P0 S2 I0 L0'
        
        Can take coil by electromagnet which simulate a solenoid.
        Need to use custom G-code commands with special prefix.
        Example: 'P0 S2 I1 L0'
        
        Z axis solenoid number: 0
        Electromagnet solenoid number: 1
        Solenoid logical lvl for move stock up: 0 
        Solenoid logical lvl for move stock down: 1  
        Electromagnet logical lvl for take a coil (turn on electro magnet): 1 
        Electromagnet logical lvl for put a coil (turn off electro magnet): 0 

    Tensioned mechanism:
        Can be rotate by servo to the angle in the range 0-180.
        Need to use custom G-code commands with special prefix.
        Example: 'P0 S1 I0 L90'

        Can go up and down by solenoid.
        Need to use custom G-code commands with special prefix.
        Example: 'P0 S2 I2 L0'

        Tensioned mechanism servo number: 0
        Tensioned mechanism solenoid number: 2
        Solenoid logical lvl for move tensioned mechanism up: 0
        Solenoid logical lvl for move tensioned mechanism down: 1



Second portal(Number: 1)

    Second portal have winder, coil mover(second block) and coil warehouse with slider(thirst block).
    This portal need to stock coils, stretch lines under fabric and wind completed fabric.

    Winder is a coil with controlled rotate(which set in moving G-command in E parameter).

    Coil mover(second block) it is a magnet platform which have controlled move on X axis.

    Coil warehouse it is the platform which have seats on the fixed positions.
    This platform can be controlled moving on Z axis.
    Seats numbered from left to right without rows attention,
    becouse they have staggered position on two rows.
                    _______________          _________________
    For example:    | 0 0 0 0 0 0 |          | 2 4 6 8 10 12 |  
                    |0 0 0 0 0 0 0|    ->    |1 3 5 7 9 11 13|    
                    ---------------          -----------------

    Slider is a small platform on coil warehouse with one solenoid.
    It need to push back some disturbing lines when coil mover move coil under the fabric.

```
     ____________________
     |   ____________   |
 Y^  |  |____________|  |
  |  |      _           |
  |  |-----|_|----------|
  |  |                  |
  |  |____==____________|
  |  ||0 0 0 0 0 0 0 0 ||
  |  || 0 0 0 0 0 0 0 0||
  |  |------------------|
  |  |__________________|
  |                      X
  |---------------------->
```

```
  ____________
 |____________| - Winder
        _
 ------|_|----- - Coil mover
 __________________
 |0 0 0 0 0 0 0 0 |
 | 0 0 0 0 0 0 0 0|  - Coil warehouse
 ------------------
 ```
       == - Slider


     Portal:
        All subsequent G-commands for head(first block) need to write with first portal prefix.

        Portal prefix: 'P1'
        Portal number: '1'


     Winder:
        Can be controlled wind by stepper motor. Don't have limited switch.
        For rotate need to use moving command with set E parameter(don't forget special prefix).
        Example: 'P1 G1 E50'


     Coil mover:
        Can be controlled move on X axis by stepper motor.
        For move need to use moving command with set X parameter(don't forget special prefix).
        Example: 'P1 G1 X20.4'


     Coil warehouse:
        Can be controlled move on Z axis by stepper motor.
        For move need to use moving command with set Z parameter(don't forget special prefix).
        Example: 'P1 G1 Z1.2'

     Slider:
        Can be controlled move on X axis by stepper motor.
        For move need to use moving command with set Y parameter(don't forget special prefix).
        Example: 'P1 G1 Y34.43'

        Can be move on Z axis by solenoid into two position(up/down).
        Need to use custom G-code commands with special prefix.
        Example: 'P1 S2 I0 L0'

        Z axis solenoid number: 0
        Solenoid logical lvl for move slider up: 1
        Solenoid logical lvl for move slider down: 0  