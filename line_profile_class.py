class Line_profile:
    def __init__(self, profile_name: str, line_diameter: float, knot_len: float, knot_width: float,
                 need_len_for_one_knot: float):
        self.profile_name = profile_name
        self.line_diameter = line_diameter
        self.knot_len = knot_len
        self.knot_width = knot_width
        self.need_len_for_one_knot = need_len_for_one_knot

    def need_rows_to_len(self, length: float):
        return length / (self.knot_len * 0.9)

    def calculate_final_width(self, use_lines_count: int):
        return (use_lines_count - 1) * self.knot_width * 0.9
