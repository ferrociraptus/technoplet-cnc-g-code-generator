from PyQt5.QtCore import QObject, pyqtSignal


class Signals_emitter(QObject):
    no_profile = pyqtSignal()

    new_string_from_serial = pyqtSignal(str)
    sent_g_code_string = pyqtSignal(str)
    error_serial_connection = pyqtSignal()
    serial_connection_done = pyqtSignal()
    serial_disconnection_done = pyqtSignal()
    serial_is_not_connect = pyqtSignal()
    some_parameter_was_change = pyqtSignal()
    serial_thread_was_finished = pyqtSignal()

    def emit_no_profile(self):
        self.no_profile.emit()

    def emit_some_parameter_was_change(self):
        self.some_parameter_was_change.emit()

    @staticmethod
    def emit_signal(signal: pyqtSignal, *args):
        signal.emit(*args)