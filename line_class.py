from PyQt5 import QtWidgets, QtGui

from point_class import Point
from knot_class import Knot
from color_class import Color

from random import randint

from resource_path import resource_path


class Line:

    def __init__(self, grid=None, grid_layout=None, begin_position_x: int = None, begin_position_y: int = None):
        self.button_for_set_color = QtWidgets.QPushButton(grid)
        self.line_knots = []
        self.line_positions = []

        if begin_position_x is not None and begin_position_y is not None and grid is not None and grid_layout is not None:
            self.line_positions.append(Point(begin_position_x, begin_position_y))
            self.grid_layout = grid_layout

        else:
            if not (grid is None and begin_position_x is None and begin_position_y is None and grid_layout is None):
                raise ValueError('Not full input')

        self.color = Color('#ffffff')
        if grid is not None and grid_layout is not None:
            self.__change_color_button_in_init()
            self.grid_layout.addWidget(self.button_for_set_color, self.line_positions[0].x, self.line_positions[0].y)

    def __change_color_button_in_init(self):
        self.button_for_set_color.setStyleSheet(
            "border-image: url({}); background-color: {};"
            .format(
                resource_path('images/Select_color_icon.svg'),
                self.color.color_in_hex('#')
            )
        )
        self.button_for_set_color.setFixedSize(50, 50)
        self.button_for_set_color.setMask(QtGui.QRegion(self.button_for_set_color.rect(), QtGui.QRegion.Ellipse))
        self.button_for_set_color.clicked.connect(self.__show_color_dialog_window)

    def change_color(self, red: int or Color = None, green: int = None, blue: int = None):
        # Change knot background color(if rgb then convert to hex())
        self.color.change_color(red, green, blue)
        self.change_icon_color()
        self.update_knot_color()

    def add_knot(self, knot: Knot):
        # Add knot to line knots:
        self.line_knots.append(knot)
        self.line_positions.append(Point(knot.position.x, knot.position.y))

    def remove_knot(self, knot: Knot):
        # Remove all knots after selected knot
        if knot in self.line_knots:
            for knot in range(self.line_knots.index(knot), len(self.line_knots)):
                self.line_knots.pop(knot)

            for position in range(self.line_positions.index(knot.position), len(self.line_positions)):
                self.line_positions.pop(position)

        else:
            raise IndexError("Knot isn't belong to line.")

    def knots_after_knot(self, knot) -> GeneratorExit:
        # Generator of all line knots after selected knot
        for knot in self.line_knots[self.line_knots.index(knot):]:
            yield knot

    def add_line_position(self, point: Point or int, y: None or int):
        if type(point) == Point:
            self.line_positions.append(point)

        else:
            if type(y) == int and type(point) == int:
                self.line_positions.append(Point(point, y))

            else:
                if type(point) != int:
                    raise ValueError('Incorrect type of input')
                else:
                    raise SyntaxWarning('Incorrect input')

    def __show_color_dialog_window(self):
        color = QtWidgets.QColorDialog.getColor()
        if color.isValid():
            self.change_icon_color()
            self.color.change_color(str(color.name()))
            self.change_color(self.color)

    def change_icon_color(self):
        self.button_for_set_color.setStyleSheet(
            "border-image: url({}); background-color: {};"
            .format(
                resource_path('images/Select_color_icon.svg'),
                self.color.color_in_hex('#')
            )
        )

    def update_knot_color(self):
        for knot in self.line_knots:
            knot.change_color(self.color)

    def clean(self):
        start_position = self.line_positions[0]
        self.line_positions.clear()
        self.line_knots.clear()
        self.line_positions.append(start_position)

    def set_random_color(self):
        self.color = Color(randint(0, 255), randint(0, 255), randint(0, 255))
        self.change_color(self.color)
        self.update_knot_color()
