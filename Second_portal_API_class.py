from G_code_class import G_code_generator


class Second_portal_API(G_code_generator):

    def auto_home(self):
        self.auto_home_axis('P1')
        self.restart_servos_or_solenoids('P1')

    def coil_warehouse_init(self, warehouse_up_position: float, warehouse_down_position: float):
        self.warehouse_up = warehouse_up_position
        self.warehouse_down = warehouse_down_position

    def coil_warehouse_up(self):
        self.move_to_position('P1', None, None, self.warehouse_up,)

    def coil_warehouse_down(self):
        self.move_to_position('P1', None, None, self.warehouse_down,)

    def move_slider(self, position: float):
        self.move_to_position('P1', None, position)

    def move_coil_mover(self, position: float):
        self.move_to_position('P1', position)

    def slider_up(self):
        self.solenoid_move('P1', 0, 1)

    def slider_down(self):
        self.solenoid_move('P1', 0, 0)

    def wind_fabric(self, length:float):
        self.move_to_position('P1', None, None, None, length)

