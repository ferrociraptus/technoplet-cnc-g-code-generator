from typing import List

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt, QObject, pyqtSignal
from PyQt5.QtGui import QDoubleValidator, QIcon, QFont

from fabric_class import Fabric
from Technoplet_API_class import Technoplet_API
from resource_path import resource_path
from signals_emitter import Signals_emitter
from show_file_dialog_window_class import Show_file_Dialog
from show_lines_profiles_dialog import Show_lines_profiles_Dialog
from Serial_thread_class import Serial_thread
from serial_ports_qcombobox import Serial_ports_QComboBox


class Interface_Ui(QObject):
    g_code_was_generated = pyqtSignal()
    __floatValidator = QDoubleValidator(-1000.0, 1000.0, 2)
    __positive_floatValidator = QDoubleValidator(0.0, 1000.0, 2)
    __lineEditWidth = 50
    __red = "background-color: rgb(200, 0, 0);"
    __gray = "background-color: rgb(220, 220, 220);"
    __yellow = "background-color: rgb(250, 255, 180);"
    __green = "background-color: rgb(43, 255, 28);"
    __white = "background-color: rgb(255, 255, 255);"

    def __init__(self, obj: QtWidgets.QMainWindow):
        # self.scroll_area = QScrollArea()
        super().__init__()
        self.object = obj
        self.serial_thread = Serial_thread()
        self.serial_thread.signal_emitter.new_string_from_serial.connect(self.__new_string_from_serial_handler)
        self.serial_thread.signal_emitter.sent_g_code_string.connect(self.__sent_g_code_string_handler)
        self.serial_thread.signal_emitter.error_serial_connection.connect(self.__error_serial_connection_handler)
        self.serial_thread.signal_emitter.serial_connection_done.connect(self.__serial_connection_done_handler)
        self.serial_thread.signal_emitter.serial_disconnection_done.connect(self.__serial_disconnection_done)
        self.serial_thread.signal_emitter.serial_is_not_connect.connect(self.__serial_is_not_connect_handler)
        self.central_widget = QtWidgets.QWidget(self.object)
        self.gridLayout_for_widgets_organisation = QtWidgets.QGridLayout(self.central_widget)
        self.signals_emitter = Signals_emitter()
        self.signals_emitter.some_parameter_was_change.connect(self.__some_parameter_was_change_handler)
        self.g_code_was_generated.connect(self.__g_code_was_generated_handler)
        self.signals_emitter.serial_thread_was_finished.connect(self.__serial_thread_finished_handler)
        self.window_with_text_about_g_code = Show_file_Dialog()

        self.thread_finish = False

        self.not_all_serial_parameters_error_message = QtWidgets.QErrorMessage()
        self.error_serial_connection_massage = QtWidgets.QErrorMessage()
        self.serial_is_not_connect_error_massage = QtWidgets.QErrorMessage()

        self.connection_to_serial_done_flag = False
        self.g_code_was_load = False

    def init_ui(self):
        self.__main_window_make_up()

        # Make object of Fabric class for organisation work with virtual fabric scheme
        self.__create_ScrollArea_for_fabric()
        self.fabric = Fabric(self.scrollAreaWidgetContents, 5, 6)
        self.technoplet_API = Technoplet_API(self.fabric.knots)
        self.technoplet_API.signals_emitter.no_profile.connect(self.__no_profiles_handler)
        self.show_line_profiles_dialog_window = Show_lines_profiles_Dialog(self.technoplet_API.line_profiles)
        self.show_line_profiles_dialog_window.show_lines_profiles_dialog_close.connect(
            self.__show_lines_profiles_dialog_close_handler)
        # self.technoplet_API.no_profile.connect(self.__no_profiles_handler)
        # -----END-----

        self.__create_add_dell_rows_and_lines_block()
        self.__create_options_block_with_Serial_port()
        self.__create_calculate_g_code_block_with_saving()
        self.__organise_widgets_on_main_window()

    def __create_options_block_with_Serial_port(self):
        # Make options block with Serial connection and G-code generator
        self.options_block_with_Serial_port = QtWidgets.QTabWidget()
        self.options_block_with_Serial_port.setStyleSheet("color: black;")
        self.options_block_with_Serial_port.setFixedWidth(320)
        # -----END-----
        self.__create_first_options_page()
        self.__create_second_serial_port_page()
        self.options_block_with_Serial_port.addTab(self.first_options_page_widget, 'Options')
        self.options_block_with_Serial_port.addTab(self.second_serial_port_page_widget, 'Serial port')

    def __create_first_options_page(self):
        self.first_options_page_widget = QtWidgets.QWidget()

        self.grid_layout_for_first_options_page = QtWidgets.QGridLayout(self.first_options_page_widget)

        self.__create_fabric_settings_block()

        self.first_options_page_ToolBox = QtWidgets.QToolBox()
        self.__create_page_with_frames_parameters()
        self.__create_page_with_g_code_calculating_parameters()
        self.__create_page_with_g_code_calculating_modes()
        self.__create_page_with_start_g_code()
        self.first_options_page_ToolBox.addItem(self.tab_widget_for_frames_options, 'Portals settings')
        self.first_options_page_ToolBox.addItem(self.tool_box_page_with_g_code_calculating_parameters,
                                                'G-code calculating parameters')
        self.first_options_page_ToolBox.addItem(self.page_with_g_code_calculating_modes_widget,
                                                'G-code calculating modes')
        self.first_options_page_ToolBox.addItem(self.page_with_start_g_code_widget, 'Start G-code')

        self.grid_layout_for_first_options_page.addWidget(self.fabric_settings_block_widget, 0, 0, 1, 1)
        self.grid_layout_for_first_options_page.addWidget(self.first_options_page_ToolBox, 1, 0, 1, 1)

    def __create_fabric_settings_block(self):
        self.fabric_settings_block_widget = QtWidgets.QWidget()
        self.fabric_settings_block_widget.setFixedHeight(100)

        self.grid_layout_for_fabric_settings_block = QtWidgets.QGridLayout(self.fabric_settings_block_widget)

        self.head_label_for_fabric_settings_block = QtWidgets.QLabel()
        self.head_label_for_fabric_settings_block.setText('Fabric settings')
        self.head_label_for_fabric_settings_block.setFont(QFont("Times New Roman", 14, QFont.Bold))

        self.lines_profiles_label = QtWidgets.QLabel()
        self.lines_profiles_label.setText('Line profile:')

        self.combo_box_for_lines_profiles = QtWidgets.QComboBox()
        for line_profile in self.technoplet_API.line_profiles:
            self.combo_box_for_lines_profiles.addItem(line_profile.profile_name)
        self.combo_box_for_lines_profiles.setMinimumWidth(100)
        self.combo_box_for_lines_profiles.currentIndexChanged.connect(self.__combo_box_for_lines_profiles_handler)

        self.show_line_profiles_button = QtWidgets.QPushButton()
        self.show_line_profiles_button.setText('...')
        self.show_line_profiles_button.setMaximumWidth(30)
        self.show_line_profiles_button.clicked.connect(self.__show_line_profiles_button_handler)

        self.lines_count_label = QtWidgets.QLabel()
        self.lines_count_label.setText('Maximum lines:')

        self.lines_count_line_edit = Interface_Ui.__create_positive_float_line_edit(
            self.technoplet_API.line_counts, self.__lines_count_line_edit_handler)

        self.fabric_len_label = QtWidgets.QLabel()
        self.fabric_len_label.setText('Fabric length:')

        self.fabric_len_line_edit = Interface_Ui.__create_positive_float_line_edit(
            self.technoplet_API.length_of_fabric, self.__fabric_len_line_edit_handler)

        self.grid_layout_for_fabric_settings_block.addWidget(self.head_label_for_fabric_settings_block, 0, 0, 1, 3)
        self.grid_layout_for_fabric_settings_block.addWidget(self.lines_count_label, 1, 0, 1, 2)
        self.grid_layout_for_fabric_settings_block.addWidget(self.lines_count_line_edit, 1, 2, 1, 1)
        self.grid_layout_for_fabric_settings_block.addWidget(self.fabric_len_label, 2, 0, 1, 2)
        self.grid_layout_for_fabric_settings_block.addWidget(self.fabric_len_line_edit, 2, 2, 1, 1)
        self.grid_layout_for_fabric_settings_block.addWidget(self.lines_profiles_label, 3, 0, 1, 2)
        self.grid_layout_for_fabric_settings_block.addWidget(self.combo_box_for_lines_profiles, 3, 1, 1, 1)
        self.grid_layout_for_fabric_settings_block.addWidget(self.show_line_profiles_button, 3, 2, 1, 1)

    def __create_page_with_frames_parameters(self):
        self.tab_widget_for_frames_options = QtWidgets.QTabWidget()
        self.tab_widget_for_frames_options.setStyleSheet("background-color: rgb(150, 150, 150);")
        self.__create_line_edit_widgets_for_frames_parameters()
        self.__create_first_portal_parameters_page()
        self.__create_second_portal_options_page()
        self.tab_widget_for_frames_options.addTab(self.first_portal_parameters_page_tool_box_widget, 'First portal')
        self.tab_widget_for_frames_options.addTab(self.second_portal_parameters_page_tool_box_widget, 'Second portal')

    def __create_first_portal_parameters_page(self):
        self.first_portal_parameters_page_tool_box_widget = QtWidgets.QToolBox()

        # Tool box page with max positions parameters
        self.first_portal_options_page_with_max_positions_widget = Interface_Ui.__create_page_widget()
        self.form_layout_for_first_frame_parameters_max_positions = QtWidgets. \
            QFormLayout(self.first_portal_options_page_with_max_positions_widget)
        self.form_layout_for_first_frame_parameters_max_positions.addRow('Max X position:',
                                                                         self.first_portal_max_x_pos_line_edit)
        self.form_layout_for_first_frame_parameters_max_positions.addRow('Max Y position:',
                                                                         self.first_portal_max_y_pos_line_edit)
        self.first_portal_parameters_page_tool_box_widget.addItem(
            self.first_portal_options_page_with_max_positions_widget,
            'Max positions')
        # Tool box page with offsets parameters
        self.first_portal_options_page_with_offsets_widget = Interface_Ui.__create_page_widget()
        self.form_layout_for_first_frame_parameters_offsets = QtWidgets.QFormLayout(
            self.first_portal_options_page_with_offsets_widget)

        self.form_layout_for_first_frame_parameters_offsets.addRow('X offset:', self.first_portal_x_offset_line_edit)
        self.form_layout_for_first_frame_parameters_offsets.addRow('Y offset:', self.first_portal_y_offset_line_edit)
        self.first_portal_parameters_page_tool_box_widget.addItem(self.first_portal_options_page_with_offsets_widget,
                                                                  'Offsets')

        # Tool box page with fedrate parameters
        self.first_portal_parameters_page_with_fedrate_widget = Interface_Ui.__create_page_widget()
        self.form_layout_for_first_frame_parameters_fedrate = QtWidgets.QFormLayout(
            self.first_portal_parameters_page_with_fedrate_widget)
        self.form_layout_for_first_frame_parameters_fedrate.addRow('Fedrate:',
                                                                   self.first_portal_standard_fedrate_line_edit)
        self.first_portal_parameters_page_tool_box_widget.addItem(self.first_portal_parameters_page_with_fedrate_widget,
                                                                  'Fedrate')

    def __create_second_portal_options_page(self):
        self.second_portal_parameters_page_tool_box_widget = QtWidgets.QToolBox()

        # Tool box page with max positions parameters
        self.second_portal_options_page_with_max_positions_widget = Interface_Ui.__create_page_widget()
        self.form_layout_for_second_frame_parameters_max_positions = QtWidgets. \
            QFormLayout(self.second_portal_options_page_with_max_positions_widget)
        self.form_layout_for_second_frame_parameters_max_positions.addRow('Max coil mover position:',
                                                                          self.second_portal_max_coil_mover_pos_line_edit)
        self.form_layout_for_second_frame_parameters_max_positions.addRow('Max slider position:',
                                                                          self.second_portal_max_slider_pos_line_edit)
        self.form_layout_for_second_frame_parameters_max_positions.addRow('Max coil warehouse position:',
                                                                          self.second_portal_max_coil_warehouse_pos_line_edit)
        self.second_portal_parameters_page_tool_box_widget.addItem(
            self.second_portal_options_page_with_max_positions_widget,
            'Max positions:')

        # Tool box page with offsets parameters
        self.second_portal_parameters_page_with_offsets_widget = Interface_Ui.__create_page_widget()
        self.form_layout_for_second_frame_parameters_offsets = QtWidgets.QFormLayout(
            self.second_portal_parameters_page_with_offsets_widget)
        self.form_layout_for_second_frame_parameters_offsets.addRow('Coil mover offset:',
                                                                    self.second_portal_coil_mover_offset_line_edit)
        self.form_layout_for_second_frame_parameters_offsets.addRow('Slider offset:',
                                                                    self.second_portal_slider_offset_line_edit)
        self.second_portal_parameters_page_tool_box_widget.addItem(
            self.second_portal_parameters_page_with_offsets_widget,
            'Offsets')

        # Tool box page with fedrate parameters
        self.second_portal_options_page_with_fedrate_widget = Interface_Ui.__create_page_widget()
        self.form_layout_for_second_frame_parameters_fedrate = QtWidgets.QFormLayout(
            self.second_portal_options_page_with_fedrate_widget)
        self.form_layout_for_second_frame_parameters_fedrate.addRow('Fedrate:',
                                                                    self.second_portal_standard_fedrate_line_edit)
        self.second_portal_parameters_page_tool_box_widget.addItem(self.second_portal_options_page_with_fedrate_widget,
                                                                   'Fedrate')

    def __create_line_edit_widgets_for_frames_parameters(self):
        # self.first_portal_max_x_pos_line_edit = self.__create_float_line_edit(
        #     str(self.technoplet_API.first_portal_max_x_pos),
        #     self.__first_portal_max_x_pos_line_edit_handler)
        self.first_portal_max_x_pos_line_edit = QtWidgets.QLineEdit()
        self.first_portal_max_x_pos_line_edit.setFixedWidth(Interface_Ui.__lineEditWidth)
        self.first_portal_max_x_pos_line_edit.setValidator(Interface_Ui.__floatValidator)
        self.first_portal_max_x_pos_line_edit.setText(str(self.technoplet_API.first_portal_max_x_pos))
        self.first_portal_max_x_pos_line_edit.setStyleSheet("background-color: rgb(245, 245, 245);")
        self.first_portal_max_x_pos_line_edit.editingFinished.connect(self.__first_portal_max_x_pos_line_edit_handler)

        self.first_portal_max_y_pos_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.first_portal_max_y_pos),
            self.__first_portal_max_y_pos_line_edit_handler)

        self.first_portal_x_offset_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.first_portal_x_offset),
            self.__first_portal_x_offset_line_edit_handler)

        self.first_portal_y_offset_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.first_portal_y_offset),
            self.__first_portal_y_offset_line_edit_handler)

        self.first_portal_standard_fedrate_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.first_portal_standard_fedrate),
            self.__first_portal_standard_fedrate_line_edit_handler)

        self.second_portal_max_coil_mover_pos_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.second_portal_max_coil_mover_pos),
            self.__second_portal_max_coil_mover_pos_line_edit_handler)

        self.second_portal_max_slider_pos_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.second_portal_max_slider_pos),
            self.__second_portal_max_slider_pos_line_edit_handler)

        self.second_portal_max_coil_warehouse_pos_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.second_portal_max_coil_warehouse_pos),
            self.__second_portal_max_coil_warehouse_pos_line_edit_handler)

        self.second_portal_coil_mover_offset_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.second_portal_coil_mover_offset),
            self.__second_portal_coil_mover_offset_line_edit_handler)

        self.second_portal_slider_offset_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.second_portal_slider_offset),
            self.__second_portal_slider_offset_line_edit_handler)

        self.second_portal_standard_fedrate_line_edit = self.__create_float_line_edit(
            str(self.technoplet_API.second_portal_standard_fedrate),
            self.__second_portal_standard_fedrate_line_edit_handler
        )

    def __create_page_with_g_code_calculating_parameters(self):
        self.tool_box_page_with_g_code_calculating_parameters = QtWidgets.QToolBox()
        self.tool_box_page_with_g_code_calculating_parameters.setStyleSheet("background-color: rgb(150, 150, 150);")
        # self.tool_box_page_with_g_code_calculating_parameters.setContentsMargins(20, 2, 2, 2)
        self.__create_line_edit_widgets_for_g_code_calculating_parameters()

        # Tool box page with positions for change coils
        self.tool_box_page_with_positions_for_change_coils_widget = Interface_Ui.__create_page_widget()

        self.form_layout_for_positions_for_change_coils = QtWidgets.QFormLayout(
            self.tool_box_page_with_positions_for_change_coils_widget)
        self.form_layout_for_positions_for_change_coils.addRow('Coil mover position:',
                                                               self.coil_mover_position_for_change_coils_line_edit)

        self.tool_box_page_with_g_code_calculating_parameters.addItem(
            self.tool_box_page_with_positions_for_change_coils_widget, 'Positions for change coil')

        # Tool box page with coil seats distances
        self.tool_box_page_with_coil_seats_distances_widget = Interface_Ui.__create_page_widget()

        self.form_layout_for_coil_seats_distances = QtWidgets.QFormLayout(
            self.tool_box_page_with_coil_seats_distances_widget)
        self.form_layout_for_coil_seats_distances.addRow('X distance:', self.coil_seats_distance_x_line_edit)
        self.form_layout_for_coil_seats_distances.addRow('Y distance:', self.coil_seats_distance_y_line_edit)

        self.tool_box_page_with_g_code_calculating_parameters.addItem(
            self.tool_box_page_with_coil_seats_distances_widget, 'Coil seats distances')

        # Tool box page with tensing angels
        self.tool_box_page_with_tensing_angels_widget = Interface_Ui.__create_page_widget()

        self.form_layout_for_tensing_angels = QtWidgets.QFormLayout(self.tool_box_page_with_tensing_angels_widget)
        self.form_layout_for_tensing_angels.addRow('Tensing left:', self.angle_for_tensioned_to_left_line_edit)
        self.form_layout_for_tensing_angels.addRow('Tensing right:', self.angle_for_tensioned_to_right_line_edit)
        self.form_layout_for_tensing_angels.addRow('Home angle:', self.angle_for_homing_tensioned_mechanism_line_edit)

        self.tool_box_page_with_g_code_calculating_parameters.addItem(self.tool_box_page_with_tensing_angels_widget,
                                                                      'Tensing line angles')

        # Tool box page with coil mover distances
        self.tool_box_page_with_coil_mover_distances_widget = Interface_Ui.__create_page_widget()

        self.form_layout_for_mover_distances = QtWidgets.QFormLayout(
            self.tool_box_page_with_coil_mover_distances_widget)
        self.form_layout_for_mover_distances.addRow('Distance from home to line:',
                                                    self.second_portal_distance_from_coil_mover_to_line_line_edit)
        self.form_layout_for_mover_distances.addRow('Distance from lines:',
                                                    self.distance_from_lines_in_coil_mover_work_area_line_edit)

        self.tool_box_page_with_g_code_calculating_parameters.addItem(
            self.tool_box_page_with_coil_mover_distances_widget, 'Coil mover distances')

        # Tool box page with slider distances
        self.tool_box_page_with_slider_distances_widget = Interface_Ui.__create_page_widget()

        self.form_layout_slider_distances = QtWidgets.QFormLayout(self.tool_box_page_with_slider_distances_widget)
        self.form_layout_slider_distances.addRow('Distance from home to line:',
                                                 self.second_portal_distance_from_slider_to_line_line_edit)
        self.form_layout_slider_distances.addRow('Distance from lines:',
                                                 self.distance_from_lines_in_slider_work_area_line_edit)

        self.tool_box_page_with_g_code_calculating_parameters.addItem(self.tool_box_page_with_slider_distances_widget,
                                                                      'Slider distances')

        # Tool box page with coil warehouse positions
        self.tool_box_page_with_coil_warehouse_positions_widget = Interface_Ui.__create_page_widget()

        self.form_layout_for_warehouse_positions = QtWidgets.QFormLayout(
            self.tool_box_page_with_coil_warehouse_positions_widget)
        self.form_layout_for_warehouse_positions.addRow('Up position:', self.coil_warehouse_up_pos_line_edit)
        self.form_layout_for_warehouse_positions.addRow('Down position:', self.coil_warehouse_down_pos_line_edit)

        self.tool_box_page_with_g_code_calculating_parameters.addItem(
            self.tool_box_page_with_coil_warehouse_positions_widget,
            'Coil warehouse positions')

        # Tool box page with first coil seat position
        self.tool_box_page_with_first_coil_seat_position_widget = Interface_Ui.__create_page_widget()

        self.form_layout_for_first_coil_seat_position = QtWidgets.QFormLayout(
            self.tool_box_page_with_first_coil_seat_position_widget)
        self.form_layout_for_first_coil_seat_position.addRow('X position:',
                                                             self.first_coil_warehouse_seat_x_pos_line_edit)
        self.form_layout_for_first_coil_seat_position.addRow('Y position:',
                                                             self.first_coil_warehouse_seat_y_pos_line_edit)

        self.tool_box_page_with_g_code_calculating_parameters.addItem(
            self.tool_box_page_with_first_coil_seat_position_widget, 'First coil seat')

        # Tool box page with tightening knot distance
        self.tool_box_page_with_tightening_knot_distance_widget = Interface_Ui.__create_page_widget()

        self.form_layout_for_knot_tightening_distance = QtWidgets.QFormLayout(
            self.tool_box_page_with_tightening_knot_distance_widget)
        self.form_layout_for_knot_tightening_distance.addRow('Distance:',
                                                             self.knot_tightening_distance_line_edit)

        self.tool_box_page_with_g_code_calculating_parameters.addItem(
            self.tool_box_page_with_tightening_knot_distance_widget, 'Knot tightening distance')

    def __create_line_edit_widgets_for_g_code_calculating_parameters(self):
        self.coil_mover_position_for_change_coils_line_edit = self.__create_float_line_edit(
            self.technoplet_API.coil_mover_position_for_change_coils,
            self.__coil_mover_position_for_change_coils_line_edit_handler)

        self.coil_seats_distance_x_line_edit = self.__create_float_line_edit(
            self.technoplet_API.coil_seats_distance_x, self.__coil_seats_distance_x_line_edit_handler)

        self.coil_seats_distance_y_line_edit = self.__create_float_line_edit(
            self.technoplet_API.coil_seats_distance_y, self.__coil_seats_distance_y_line_edit_handler)

        self.angle_for_tensioned_to_left_line_edit = self.__create_float_line_edit(
            self.technoplet_API.angle_for_tensioned_to_left, self.__angle_for_tensioned_to_left_line_edit_handler)

        self.angle_for_tensioned_to_right_line_edit = self.__create_float_line_edit(
            self.technoplet_API.angle_for_tensioned_to_right, self.__angle_for_tensioned_to_right_line_edit_handler)

        self.angle_for_homing_tensioned_mechanism_line_edit = self.__create_float_line_edit(
            self.technoplet_API.angle_for_homing_tensioned_mechanism,
            self.__angle_for_homing_tensioned_mechanism_line_edit_handler)

        self.second_portal_distance_from_coil_mover_to_line_line_edit = self.__create_float_line_edit(
            self.technoplet_API.second_portal_distance_from_coil_mover_to_line,
            self.__second_portal_distance_from_coil_mover_to_line_line_edit_handler)

        self.second_portal_distance_from_slider_to_line_line_edit = self.__create_float_line_edit(
            self.technoplet_API.second_portal_distance_from_slider_to_line,
            self.__second_portal_distance_from_slider_to_line_line_edit_handler)

        self.coil_warehouse_up_pos_line_edit = self.__create_float_line_edit(
            self.technoplet_API.coil_warehouse_up_pos, self.__coil_warehouse_up_pos_line_edit_handler)

        self.coil_warehouse_down_pos_line_edit = self.__create_float_line_edit(
            self.technoplet_API.coil_warehouse_down_pos, self.__coil_warehouse_down_pos_line_edit_handler)

        self.first_coil_warehouse_seat_x_pos_line_edit = self.__create_float_line_edit(
            self.technoplet_API.first_coil_warehouse_seat_x_pos, self.__first_coil_warehouse_seat_x_pos_line_edit_handler)

        self.first_coil_warehouse_seat_y_pos_line_edit = self.__create_float_line_edit(
            self.technoplet_API.first_coil_warehouse_seat_y_pos, self.__first_coil_warehouse_seat_y_pos_line_edit_handler)

        self.distance_from_lines_in_slider_work_area_line_edit = self.__create_float_line_edit(
            self.technoplet_API.distance_from_lines_in_slider_work_area,
            self.__distance_from_lines_in_slider_work_area_line_edit_handler)

        self.distance_from_lines_in_coil_mover_work_area_line_edit = self.__create_float_line_edit(
            self.technoplet_API.distance_from_lines_in_coil_mover_work_area,
            self.__distance_from_lines_in_coil_mover_work_area_line_edit_handler)

        self.knot_tightening_distance_line_edit = self.__create_float_line_edit(
            self.technoplet_API.knot_tightening_distance, self.__knot_tightening_distance_line_edit_handler)

        self.first_portal_y_position_to_coil_mover_line_edit = self.__create_float_line_edit(
            self.technoplet_API.first_portal_y_position_to_coil_mover,
            self.__first_portal_y_position_to_coil_mover_line_edit_handler)

        self.lines_shift_length_line_edit = self.__create_float_line_edit(
            self.technoplet_API.lines_shift_length, self.__lines_shift_length_line_edit_handler)

    def __create_page_with_g_code_calculating_modes(self):
        self.page_with_g_code_calculating_modes_widget = QtWidgets.QWidget()
        self.page_with_g_code_calculating_modes_widget.setStyleSheet("background-color: rgb(170, 170, 170);")
        self.page_with_g_code_calculating_modes_widget.setFixedHeight(80)

        self.grid_layout_for_g_code_calculating_modes = QtWidgets.QGridLayout(
            self.page_with_g_code_calculating_modes_widget)

        self.comments_in_final_g_code_mode_check_box = QtWidgets.QCheckBox('Comments in G-code')
        self.comments_in_final_g_code_mode_check_box.setStyleSheet("background-color: rgb(220, 220, 220);")
        if self.technoplet_API.comments_in_final_g_code:
            self.comments_in_final_g_code_mode_check_box.setChecked(True)
        self.comments_in_final_g_code_mode_check_box.stateChanged.connect(
            self.__comments_in_final_g_code_mode_check_box_handler)

        self.extreme_coil_seats_mode_check_box = QtWidgets.QCheckBox('Extreme lines mode')
        self.extreme_coil_seats_mode_check_box.setStyleSheet("background-color: rgb(220, 220, 220);")
        if self.technoplet_API.extreme_coil_seats_mode:
            self.extreme_coil_seats_mode_check_box.setChecked(True)
        self.extreme_coil_seats_mode_check_box.stateChanged.connect(
            self.__extreme_coil_seats_mode_check_box_handler)

        self.grid_layout_for_g_code_calculating_modes.addWidget(self.comments_in_final_g_code_mode_check_box,
                                                                0, 0, 1, 1)
        self.grid_layout_for_g_code_calculating_modes.addWidget(self.extreme_coil_seats_mode_check_box, 1, 0, 1, 1)

    def __create_page_with_start_g_code(self):
        self.page_with_start_g_code_widget = QtWidgets.QWidget()
        self.page_with_start_g_code_widget.setStyleSheet("background-color: rgb(150, 150, 150);")

        self.grid_layout_for_page_with_start_g_code = QtWidgets.QGridLayout(self.page_with_start_g_code_widget)

        self.text_edit_for_start_g_code_input = QtWidgets.QTextEdit()
        self.text_edit_for_start_g_code_input.setStyleSheet("background-color: rgb(220, 220, 220);")
        self.text_edit_for_start_g_code_input.setText(
            Interface_Ui.__text_from_list(self.technoplet_API.start_g_code))
        self.text_edit_for_start_g_code_input.textChanged.connect(self.__text_edit_for_start_g_code_input_handler)

        self.push_button_g_code_about = QtWidgets.QPushButton()
        self.push_button_g_code_about.setStyleSheet("background-color: rgb(220, 220, 220);")
        self.push_button_g_code_about.setText('G-code about')
        self.push_button_g_code_about.clicked.connect(self.__push_button_g_code_about_handler)

        self.grid_layout_for_page_with_start_g_code.addWidget(self.push_button_g_code_about, 0, 0, 1, 1)
        self.grid_layout_for_page_with_start_g_code.addWidget(self.text_edit_for_start_g_code_input, 1, 0, 1, 1)

    def __create_second_serial_port_page(self):
        self.second_serial_port_page_widget = QtWidgets.QWidget()

        self.grid_layout_for_second_serial_port_page = QtWidgets.QGridLayout \
            (self.second_serial_port_page_widget)

        self.__create_serial_port_settings_block()

        self.serial_port_terminal_text_edit = QtWidgets.QTextEdit()
        self.serial_port_terminal_text_edit.setReadOnly(True)
        self.serial_port_terminal_text_edit.setStyleSheet(Interface_Ui.__white)

        self.serial_port_terminal_line_edit = QtWidgets.QLineEdit()
        self.serial_port_terminal_line_edit.setStyleSheet(Interface_Ui.__white)
        self.serial_port_terminal_line_edit.editingFinished.connect(self.__serial_line_editing_finished_handler)
        self.serial_port_terminal_line_edit.setDisabled(True)

        self.serial_port_terminal_send_line_button = QtWidgets.QPushButton()
        self.serial_port_terminal_send_line_button.setText('Send')
        self.serial_port_terminal_send_line_button.setMaximumWidth(70)
        self.serial_port_terminal_send_line_button.setDisabled(True)

        self.__create_g_code_buttons_block_for_serial_page()

        self.grid_layout_for_second_serial_port_page.addWidget(self.serial_port_settings_block_widget, 0, 0, 1, 2)
        self.grid_layout_for_second_serial_port_page.addWidget(self.serial_port_terminal_text_edit, 1, 0, 1, 2)
        self.grid_layout_for_second_serial_port_page.addWidget(self.serial_port_terminal_line_edit, 2, 0, 1, 1)
        self.grid_layout_for_second_serial_port_page.addWidget(self.serial_port_terminal_send_line_button, 2, 1, 1, 1)
        self.grid_layout_for_second_serial_port_page.addWidget(self.g_code_buttons_block_to_serial_page_widget,
                                                               3, 0, 1, 2)

    def __create_serial_port_settings_block(self):
        self.serial_port_settings_block_widget = QtWidgets.QWidget()

        self.grid_for_serial_port_settings_block = QtWidgets.QGridLayout(self.serial_port_settings_block_widget)

        self.head_label_for_serial_port_settings_block = QtWidgets.QLabel()
        self.head_label_for_serial_port_settings_block.setText('Serial settings')
        self.head_label_for_serial_port_settings_block.setFont(QFont("Times New Roman", 14, QFont.Bold))

        self.serial_port_label = QtWidgets.QLabel()
        self.serial_port_label.setText('Serial port:')

        self.serial_port_combo_box = Serial_ports_QComboBox()

        self.baudrate_label = QtWidgets.QLabel()
        self.baudrate_label.setText('Baudrate:')

        self.baudrate_combo_box = QtWidgets.QComboBox()
        self.baudrate_combo_box.addItems(
            ["2400", "4800", "9600", "14400", "19200", "28800", "38400", "57600", "76800", "115200",
             "230400", "250000", "500000"])
        self.baudrate_combo_box.setCurrentIndex(9)

        self.serial_connect_button = QtWidgets.QPushButton()
        self.serial_connect_button.setText('Connect')
        self.serial_connect_button.setFont(QFont("DejaVu Sans Hard", 12, QFont.Bold))
        self.serial_connect_button.clicked.connect(self.__serial_connect_button_handler)

        self.grid_for_serial_port_settings_block.addWidget(self.head_label_for_serial_port_settings_block, 0, 0, 1, 2)
        self.grid_for_serial_port_settings_block.addWidget(self.serial_port_label, 1, 0, 1, 1)
        self.grid_for_serial_port_settings_block.addWidget(self.serial_port_combo_box, 1, 1, 1, 1)
        self.grid_for_serial_port_settings_block.addWidget(self.baudrate_label, 2, 0, 1, 1)
        self.grid_for_serial_port_settings_block.addWidget(self.baudrate_combo_box, 2, 1, 1, 1)
        self.grid_for_serial_port_settings_block.addWidget(self.serial_connect_button, 3, 1, 1, 1)

    def __create_g_code_buttons_block_for_serial_page(self):
        self.g_code_buttons_block_to_serial_page_widget = QtWidgets.QWidget()
        self.grid_layout_for_g_code_buttons_block_to_serial_page_widget = QtWidgets.QGridLayout(
            self.g_code_buttons_block_to_serial_page_widget)

        self.show_send_g_code_check_box = QtWidgets.QCheckBox('Show sent G-code')
        self.show_send_g_code_check_box.setChecked(True)
        self.show_send_g_code_check_box.setMaximumHeight(15)

        self.send_g_code_to_serail_button = QtWidgets.QPushButton()
        self.send_g_code_to_serail_button.setText('Sand G-code')
        self.send_g_code_to_serail_button.setDisabled(True)
        self.send_g_code_to_serail_button.clicked.connect(self.__send_g_code_button_handler)

        self.grid_layout_for_g_code_buttons_block_to_serial_page_widget.addWidget(self.show_send_g_code_check_box,
                                                                                  0, 1, 1, 1)
        self.grid_layout_for_g_code_buttons_block_to_serial_page_widget.addWidget(self.send_g_code_to_serail_button,
                                                                                  1, 0, 1, 2)

    def __organise_widgets_on_main_window(self):
        # Organise widgets on main window
        self.grid_layout_for_settings_column_widget = QtWidgets.QWidget()
        self.grid_layout_for_settings_column_widget.setFixedWidth(320)
        self.grid_layout_for_settings_column = QtWidgets.QGridLayout(self.grid_layout_for_settings_column_widget)

        self.grid_layout_for_settings_column.addWidget(self.options_block_with_Serial_port, 0, 0, 1, 1)
        self.grid_layout_for_settings_column.addWidget(self.calculate_g_code_block_with_saving_widget, 1, 0, 1, 1)

        self.gridLayout_for_widgets_organisation.addWidget(self.scrollArea, 0, 0, 1, 1)
        self.gridLayout_for_widgets_organisation.addWidget(self.add_del_block, 1, 0, 1, 1)
        self.gridLayout_for_widgets_organisation.addWidget(self.grid_layout_for_settings_column_widget, 0, 1, 2, 1)
        self.object.setCentralWidget(self.central_widget)
        # -----END-----

    def __create_ScrollArea_for_fabric(self):
        # Make ScrollArea for grid
        self.gridLayout_for_widgets_organisation.setContentsMargins(5, 10, 5, 10)
        self.gridLayout_for_widgets_organisation.setSpacing(5)
        self.scrollArea = QtWidgets.QScrollArea(self.central_widget)
        self.scrollArea.setWidgetResizable(True)
        self.__create_grid_widget_for_virtual_fabric_scheme()
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        # -----END-----

    def __create_grid_widget_for_virtual_fabric_scheme(self):
        # Make grid widget for virtual fabric scheme
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        # self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 403, 326))
        # -----END-----

    def __create_fabric_clean_button(self):
        # button for fabric clean
        self.clean_fabric_button = QtWidgets.QPushButton(self.object)
        self.clean_fabric_button.setText('Reset')
        # -----END-----

    def __create_set_random_color_button(self):
        # Button for set random color to lines.
        self.set_random_color_to_lines_button = QtWidgets.QPushButton(self.object)
        self.set_random_color_to_lines_button.setText('Random color')
        self.set_random_color_to_lines_button.setMinimumWidth(120)
        # -----END-----

    def __main_window_make_up(self):
        # Main window make-up
        self.object.setStyleSheet("color: black;")
        self.object.setWindowFlags(Qt.WindowStaysOnTopHint)  # Stay on top
        self.object.setObjectName("main_window")
        self.object.setEnabled(True)
        self.object.resize(450, 600)
        self.object.setContentsMargins(3, 2, 3, 2)
        self.object.setWindowTitle('Technoplet G-code generator')
        self.object.setWindowIcon(QIcon(resource_path('images/Window_icon.svg')))
        # object.showMaximized()
        self.object.setStyleSheet("background-color: rgb(200, 200, 200);")
        # -----END-----

    def __create_add_dell_rows_and_lines_block(self):
        self.__create_add_dell_rows_buttons()

        self.__create_add_dell_lines_buttons()

        self.__create_set_random_color_button()

        self.__create_fabric_clean_button()

        # Make add/del block
        self.add_del_block = QtWidgets.QWidget(self.central_widget)
        self.add_del_block.setStyleSheet("color: black;")
        self.add_del_block.setMaximumSize(450, 100)

        self.add_dell_grid_widget = QtWidgets.QGridLayout(self.add_del_block)

        self.add_dell_grid_widget.addWidget(self.add_del_rows_label, 0, 0)
        self.add_dell_grid_widget.addWidget(self.add_del_lines_label, 1, 0)

        self.add_dell_grid_widget.addWidget(self.add_row_button, 0, 1)
        self.add_dell_grid_widget.addWidget(self.add_line_button, 1, 1)

        self.add_dell_grid_widget.addWidget(self.del_row_button, 0, 2)
        self.add_dell_grid_widget.addWidget(self.del_line_button, 1, 2)

        self.add_dell_grid_widget.addWidget(self.clean_fabric_button, 0, 3, 2, 1)

        self.add_dell_grid_widget.setColumnMinimumWidth(4, 150)

        self.add_dell_grid_widget.addWidget(self.set_random_color_to_lines_button, 0, 5, 2, 2)

        self.add_dell_grid_widget.setHorizontalSpacing(5)
        # -----END-----

        self.__connect_handler_to_add_dell_buttons()

        self.__connect_handler_to_set_random_color_button()

        self.__connect_handler_to_fabric_clean_button()

    def __connect_handler_to_fabric_clean_button(self):
        # Connect handler to fabric clean button
        self.clean_fabric_button.clicked.connect(self.fabric.return_to_default)
        # -----END-----

    def __connect_handler_to_set_random_color_button(self):
        # Connect handler to set random color button
        self.set_random_color_to_lines_button.clicked.connect(self.fabric.set_random_colors_to_lines)
        # -----END-----

    def __connect_handler_to_add_dell_buttons(self):
        # Connect handler to add/del buttons
        self.add_row_button.clicked.connect(self.add_row)
        self.del_row_button.clicked.connect(self.del_row)

        self.add_line_button.clicked.connect(self.add_line)
        self.del_line_button.clicked.connect(self.del_line)
        # -----END-----

    def __create_add_dell_lines_buttons(self):
        # Buttons for add and delete line in virtual fabric scheme
        self.add_del_lines_label = QtWidgets.QLabel(self.object)
        self.add_del_lines_label.setText('Line:')
        self.add_line_button = QtWidgets.QPushButton(self.object)
        self.add_line_button.setText("Add")
        self.del_line_button = QtWidgets.QPushButton(self.object)
        self.del_line_button.setText("Del")
        # -----END-----

    def __create_add_dell_rows_buttons(self):
        # Buttons for add and delete row in virtual fabric scheme
        self.add_del_rows_label = QtWidgets.QLabel(self.object)
        self.add_del_rows_label.setText('Row:')
        self.add_row_button = QtWidgets.QPushButton(self.object)
        self.add_row_button.setText("Add")
        self.del_row_button = QtWidgets.QPushButton(self.object)
        self.del_row_button.setText("Del")
        # -----END-----

    def __create_calculate_g_code_block_with_saving(self):
        self.calculate_g_code_block_with_saving_widget = QtWidgets.QWidget(self.object)
        self.calculate_g_code_block_with_saving_widget.setStyleSheet("color: black;")

        self.grid_layout_for_calculate_g_code_block_with_saving = QtWidgets.QGridLayout(
            self.calculate_g_code_block_with_saving_widget)

        self.calculate_g_code_button = QtWidgets.QPushButton()
        self.calculate_g_code_button.setText('Calculate g code')
        self.calculate_g_code_button.setStyleSheet("background-color: rgb(200, 200, 200);")
        self.calculate_g_code_button_font = QFont("Times", 20, QFont.Bold)
        self.calculate_g_code_button.setFont(self.calculate_g_code_button_font)
        self.calculate_g_code_button.clicked.connect(self.__calculate_g_code_button_handler)

        self.save_g_code_button = QtWidgets.QPushButton()
        self.save_g_code_button.setText('Save')
        self.save_g_code_button.setDisabled(True)
        self.save_g_code_button.clicked.connect(self.__save_g_code_button_handler)

        self.load_g_code_button = QtWidgets.QPushButton()
        self.load_g_code_button.setText('Load G-code')
        self.load_g_code_button.clicked.connect(self.__load_g_code_button_handler)

        self.grid_layout_for_calculate_g_code_block_with_saving.addWidget(self.calculate_g_code_button, 0, 0, 1, 2)
        self.grid_layout_for_calculate_g_code_block_with_saving.addWidget(self.save_g_code_button, 1, 0, 1, 1)
        self.grid_layout_for_calculate_g_code_block_with_saving.addWidget(self.load_g_code_button, 1, 1, 1, 1)

    @staticmethod
    def __create_float_line_edit(start_value: str or float or int,
                                 editing_finished_handler: classmethod) -> QtWidgets.QLineEdit:
        if type(start_value) != str:
            start_value = str(start_value)
        line_edit = QtWidgets.QLineEdit()
        line_edit.setFixedWidth(Interface_Ui.__lineEditWidth)
        line_edit.setValidator(Interface_Ui.__floatValidator)
        line_edit.setText(start_value)
        line_edit.setStyleSheet("background-color: rgb(245, 245, 245);")
        line_edit.editingFinished.connect(editing_finished_handler)
        return line_edit

    @staticmethod
    def __create_positive_float_line_edit(start_value: str or float or int,
                                          editing_finished_handler: classmethod) -> QtWidgets.QLineEdit:
        if type(start_value) != str:
            start_value = str(start_value)
        line_edit = QtWidgets.QLineEdit()
        line_edit.setFixedWidth(Interface_Ui.__lineEditWidth)
        line_edit.setValidator(Interface_Ui.__positive_floatValidator)
        line_edit.setText(start_value)
        line_edit.setStyleSheet("background-color: rgb(245, 245, 245);")
        line_edit.editingFinished.connect(editing_finished_handler)
        return line_edit

    @staticmethod
    def __create_page_widget():
        page_widget = QtWidgets.QWidget()
        page_widget.setStyleSheet("background-color: rgb(220, 220, 220);")
        return page_widget

    @staticmethod
    def __text_from_list(list_of_lines: List[str]):
        text = ''
        for line in list_of_lines:
            text += line + '\n'
        return text

    # Handler methods for Qt Widgets
    def add_row(self):
        self.fabric.add_rows(1)
        self.object.update()

    def del_row(self):
        if self.fabric.rows > 1:
            self.fabric.del_rows(1)
            self.object.update()

    def add_line(self):
        if self.technoplet_API.line_counts > len(self.fabric.lines):
            self.fabric.add_lines(1)
        self.object.update()

    def del_line(self):
        if self.fabric.columns > 2:
            self.fabric.del_lines(1)
            self.object.update()

    def close_all(self):
        self.window_with_text_about_g_code.close()
        self.show_line_profiles_dialog_window.close()
        self.serial_thread.terminate_thread()

    # Handlers for line edit widgets in frames options
    def float_add_for_line_edit_input(self):
        sender: QtWidgets.QLineEdit = self.sender()
        if "." in sender.text():
            sender.setText(sender.text().replace('.', ','))


    def __first_portal_max_x_pos_line_edit_handler(self):
        self.technoplet_API.first_portal_max_x_pos = float(
            self.first_portal_max_x_pos_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __first_portal_max_y_pos_line_edit_handler(self):
        self.technoplet_API.first_portal_max_y_pos = float(
            self.first_portal_max_y_pos_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __first_portal_x_offset_line_edit_handler(self):
        self.technoplet_API.first_portal_x_offset = float(self.first_portal_x_offset_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __first_portal_y_offset_line_edit_handler(self):
        self.technoplet_API.first_portal_y_offset = float(self.first_portal_y_offset_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __first_portal_standard_fedrate_line_edit_handler(self):
        self.technoplet_API.first_portal_standard_fedrate = float(self.first_portal_standard_fedrate_line_edit.text()
                                                                  .replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __second_portal_max_coil_mover_pos_line_edit_handler(self):
        self.technoplet_API.second_portal_max_coil_mover_pos = float(
            self.second_portal_max_coil_mover_pos_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __second_portal_max_slider_pos_line_edit_handler(self):
        self.technoplet_API.second_portal_max_slider_pos = float(self.second_portal_max_slider_pos_line_edit.text()
                                                                 .replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __second_portal_max_coil_warehouse_pos_line_edit_handler(self):
        self.technoplet_API.second_portal_max_coil_warehouse_pos = float(self. \
                                                                         second_portal_max_coil_warehouse_pos_line_edit
                                                                         .text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __second_portal_coil_mover_offset_line_edit_handler(self):
        self.technoplet_API.second_portal_coil_mover_offset = float(
            self.second_portal_coil_mover_offset_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __second_portal_slider_offset_line_edit_handler(self):
        self.technoplet_API.second_portal_slider_offset = float(self.second_portal_slider_offset_line_edit.text()
                                                                .replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __second_portal_standard_fedrate_line_edit_handler(self):
        self.technoplet_API.second_portal_standard_fedrate = float(
            self.second_portal_standard_fedrate_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    # Handlers for line edit widgets in g code calculating options

    def __coil_mover_position_for_change_coils_line_edit_handler(self):
        self.technoplet_API.coil_mover_position_for_change_coils = float(self. \
                                                                         coil_mover_position_for_change_coils_line_edit
                                                                         .text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __coil_seats_distance_x_line_edit_handler(self):
        self.technoplet_API.coil_seats_distance_x = float(self.coil_seats_distance_x_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __coil_seats_distance_y_line_edit_handler(self):
        self.technoplet_API.coil_seats_distance_y = float(self.coil_warehouse_seats_distance_line_edit.text()
                                                          .replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __angle_for_tensioned_to_left_line_edit_handler(self):
        self.technoplet_API.angle_for_tensioned_to_left = float(self.angle_for_tensioned_to_left_line_edit
                                                                .text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __angle_for_tensioned_to_right_line_edit_handler(self):
        self.technoplet_API.angle_for_tensioned_to_right = float(self.angle_for_tensioned_to_right_line_edit
                                                                 .text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __angle_for_homing_tensioned_mechanism_line_edit_handler(self):
        self.technoplet_API.angle_for_homing_tensioned_mechanism = float(self. \
                                                                         angle_for_homing_tensioned_mechanism_line_edit
                                                                         .text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __second_portal_distance_from_coil_mover_to_line_line_edit_handler(self):
        self.technoplet_API.second_portal_distance_from_coil_mover_to_line = float(self. \
            second_portal_distance_from_coil_mover_to_line_line_edit.text().replace(
            ',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __second_portal_distance_from_slider_to_line_line_edit_handler(self):
        self.technoplet_API.second_portal_distance_from_slider_to_line = float(self. \
            second_portal_distance_from_slider_to_line_line_edit.text().replace(
            ',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __coil_warehouse_up_pos_line_edit_handler(self):
        self.technoplet_API.coil_warehouse_up_pos = float(self.coil_warehouse_up_pos_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __coil_warehouse_down_pos_line_edit_handler(self):
        self.technoplet_API.coil_warehouse_down_pos = float(self.coil_warehouse_down_pos_line_edit.text()
                                                            .replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __first_coil_warehouse_seat_x_pos_line_edit_handler(self):
        self.technoplet_API.first_coil_warehouse_seat_x_pos = float(
            self.first_coil_warehouse_seat_x_pos_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __first_coil_warehouse_seat_y_pos_line_edit_handler(self):
        self.technoplet_API.first_coil_warehouse_seat_y_pos = float(
            self.first_coil_warehouse_seat_y_pos_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __distance_from_lines_in_slider_work_area_line_edit_handler(self):
        self.technoplet_API.distance_from_lines_in_slider_work_area = float(self. \
                                                                            distance_from_lines_in_slider_work_area_line_edit.text()
                                                                            .replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __distance_from_lines_in_coil_mover_work_area_line_edit_handler(self):
        self.technoplet_API.distance_from_lines_in_coil_mover_work_area = float(self. \
                                                                                distance_from_lines_in_coil_mover_work_area_line_edit.text()
                                                                                .replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __knot_tightening_distance_line_edit_handler(self):
        self.technoplet_API.knot_tightening_distance = float(self.knot_tightening_distance_line_edit.text()
                                                             .replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __first_portal_y_position_to_coil_mover_line_edit_handler(self):
        self.technoplet_API.first_portal_y_position_to_coil_mover = float(
            self.first_portal_y_position_to_coil_mover_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __lines_shift_length_line_edit_handler(self):
        self.technoplet_API.lines_shift_length = float(self.lines_shift_length_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __text_edit_for_start_g_code_input_handler(self):
        self.technoplet_API.start_g_code = self.text_edit_for_start_g_code_input.toPlainText().strip().split('\n')
        self.signals_emitter.emit_some_parameter_was_change()

    def __push_button_g_code_about_handler(self):
        self.window_with_text_about_g_code = Show_file_Dialog()
        self.window_with_text_about_g_code.setFile('README.md')
        self.window_with_text_about_g_code.show()

    # Handlers for widgets in calculate G-code block with saving
    def __calculate_g_code_button_handler(self):
        self.technoplet_API.generate_g_code()
        self.signals_emitter.emit_signal(self.g_code_was_generated)
        self.g_code_was_load = False

    def __save_g_code_button_handler(self):
        file_path = QtWidgets.QFileDialog.getSaveFileName(None, "Save G-code",
                                                          '/home/untitled.gcode',
                                                          " G-code files(*.gcode);; Text files (*.txt)")
        with open(str(file_path[0]), 'w') as file:
            for line in self.technoplet_API.g_code:
                file.write(line + '\n')

    def __load_g_code_button_handler(self):
        file_path = QtWidgets.QFileDialog.getOpenFileName(None, "Load G-code",
                                                          "/home/",
                                                          " G-code files(*.gcode);; Text files (*.txt)")
        if len(file_path[0]) > 1:
            self.technoplet_API.load_g_code_from_file(str(file_path[0]))
            self.signals_emitter.emit_signal(self.g_code_was_generated)
            self.g_code_was_load = True

    # Handlers for check box in G-code calculating modes block
    def __comments_in_final_g_code_mode_check_box_handler(self, state):
        if state == QtCore.Qt.Checked:
            self.technoplet_API.comments_in_final_g_code = True
        else:
            self.technoplet_API.comments_in_final_g_code = False
        self.signals_emitter.emit_some_parameter_was_change()

    def __extreme_coil_seats_mode_check_box_handler(self, state):
        if state == QtCore.Qt.Checked:
            self.technoplet_API.extreme_coil_seats_mode = True
        else:
            self.technoplet_API.extreme_coil_seats_mode = False
        self.signals_emitter.emit_some_parameter_was_change()

    # Handlers for widgets in fabric settings block
    def __lines_count_line_edit_handler(self):
        self.technoplet_API.line_counts = float(self.lines_count_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __fabric_len_line_edit_handler(self):
        self.technoplet_API.length_of_fabric = float(self.fabric_len_line_edit.text().replace(',', '.'))
        self.signals_emitter.emit_some_parameter_was_change()

    def __show_line_profiles_button_handler(self):
        self.object.setDisabled(True)
        self.show_line_profiles_dialog_window.show()

    def __combo_box_for_lines_profiles_handler(self, index):
        self.technoplet_API.selected_line_profile = index
        self.signals_emitter.emit_some_parameter_was_change()

    # Handlers for widgets in serial settings block
    def __serial_connect_button_handler(self):
        if self.serial_thread.serial is None:
            port = self.serial_port_combo_box.currentText().strip()
            baudrate = int(self.baudrate_combo_box.currentText())
            if port != '':
                self.serial_thread.connect(port, baudrate)
            else:
                self.serial_port_combo_box.setStyleSheet(Interface_Ui.__yellow)
                self.not_all_serial_parameters_error_message.setWindowFlags(Qt.WindowStaysOnTopHint)
                self.not_all_serial_parameters_error_message.setWindowTitle('Not all parameters')
                self.not_all_serial_parameters_error_message.showMessage('Before connecting to Serial'
                                                                         ' set all need parameters')
        else:
            self.serial_thread.disconnect()

    def __send_g_code_button_handler(self):
        self.serial_thread.send_g_code(self.technoplet_API.g_code)

    def __serial_line_editing_finished_handler(self):
        terminal_line_string = self.serial_port_terminal_line_edit.text().strip()
        if terminal_line_string == 'clear':
            self.serial_port_terminal_text_edit.clear()
        elif terminal_line_string != '':
            if self.serial_thread.serial is not None:
                self.serial_port_terminal_text_edit.append('>>>' + terminal_line_string)
            self.serial_thread.send_to_serial(terminal_line_string)
        self.serial_port_terminal_line_edit.clear()

    # Signals handlers from windows
    def __show_lines_profiles_dialog_close_handler(self):
        self.technoplet_API.save_all_parameters()
        self.object.setDisabled(False)
        self.combo_box_for_lines_profiles.clear()
        for line_profile in self.technoplet_API.line_profiles:
            self.combo_box_for_lines_profiles.addItem(str(line_profile.profile_name))

    def __some_parameter_was_change_handler(self):
        self.calculate_g_code_button.setStyleSheet(Interface_Ui.__gray)
        self.technoplet_API.save_all_parameters()
        self.save_g_code_button.setDisabled(True)
        if not self.g_code_was_load:
            self.send_g_code_to_serail_button.setDisabled(True)

    def __no_profiles_handler(self):
        self.calculate_g_code_button.setStyleSheet(Interface_Ui.__red)
        self.no_profiles_error_message = QtWidgets.QErrorMessage()
        self.no_profiles_error_message.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.no_profiles_error_message.setWindowTitle('No profiles error')
        self.no_profiles_error_message.showMessage('Add line profile before calculating G-code.')
        # self.no_profiles_error_message.exec()

    def __g_code_was_generated_handler(self):
        if self.connection_to_serial_done_flag:
            self.send_g_code_to_serail_button.setDisabled(False)
        self.save_g_code_button.setDisabled(False)

    # Signals handlers from serial port thread
    def __new_string_from_serial_handler(self, string):
        self.serial_port_terminal_text_edit.append(string)

    def __sent_g_code_string_handler(self, string):
        if self.show_send_g_code_check_box.stateChanged() == QtCore.Qt.Checked:
            self.serial_port_terminal_text_edit.append(string)

    def __error_serial_connection_handler(self):
        self.connection_to_serial_done_flag = False
        self.error_serial_connection_massage.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.error_serial_connection_massage.setWindowTitle('Error connection')
        self.error_serial_connection_massage.showMessage('Something was wrong. Try connection again.')

    def __serial_connection_done_handler(self):
        self.connection_to_serial_done_flag = True
        self.serial_connect_button.setStyleSheet(Interface_Ui.__green)
        self.serial_port_terminal_line_edit.setDisabled(False)
        self.serial_port_terminal_send_line_button.setDisabled(False)
        self.serial_connect_button.setText('Disconnect')

    def __serial_disconnection_done(self):
        self.connection_to_serial_done_flag = False
        self.serial_connect_button.setStyleSheet(Interface_Ui.__gray)
        self.serial_connect_button.setText('Connect')
        self.serial_port_terminal_line_edit.setDisabled(True)
        self.serial_port_terminal_send_line_button.setDisabled(True)

    def __serial_is_not_connect_handler(self):
        self.serial_is_not_connect_error_massage.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.serial_is_not_connect_error_massage.setWindowTitle('Device is not connect.')
        self.serial_is_not_connect_error_massage.showMessage('Connect to devise before send command or G-code.')

    def __serial_thread_finished_handler(self):
        self.thread_finish = True
