from threading import Thread
from typing import List
import serial
from time import sleep
from signals_emitter import Signals_emitter

functions_sequence = [[], [], []]


def synchronize_in_thread(function):
    global functions_sequence

    def wrapper(*args, **kwargs):
        functions_sequence[0].append(function)
        functions_sequence[1].append(args)
        functions_sequence[2].append(kwargs)
        return None
    return wrapper


class Serial_thread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.signal_emitter = Signals_emitter()
        self.run_available = True
        self.serial = None
        self.name = "Serial-connect-thread"
        self.start()

    def run(self) -> None:
        global functions_sequence

        self.run_available = True

        while self.run_available:
            sleep(0.05)

            if len(functions_sequence[0]) > 0:
                function = functions_sequence[0].pop(0)
                arguments = functions_sequence[1].pop(0)
                key_word_arguments = functions_sequence[2].pop(0)
                function(*arguments, **key_word_arguments)

            if self.serial is not None:
                if not self.serial.is_open:
                    self.serial.open()

                while self.serial.in_waiting:
                    try:
                        data: bytes = self.serial.readline()

                        if type(data) == bytes:
                            data: str = data.decode('utf-8')

                        if data != "":
                            self.signal_emitter.emit_signal(self.signal_emitter.new_string_from_serial, data.strip())
                    except:
                        self.signal_emitter.emit_signal(self.signal_emitter.error_serial_connection)
                        return None
        self.signal_emitter.emit_signal(self.signal_emitter.serial_thread_was_finished)

    @synchronize_in_thread
    def connect(self, port: str, baudrate: int):
        try:
            self.serial = serial.Serial(port, baudrate, timeout=0.2)
            self.signal_emitter.emit_signal(self.signal_emitter.serial_connection_done)
        except serial.SerialException:
            self.signal_emitter.emit_signal(self.signal_emitter.error_serial_connection)

    @synchronize_in_thread
    def disconnect(self):
        if self.serial is not None:
            self.serial.close()
            self.serial = None
            self.signal_emitter.emit_signal(self.signal_emitter.serial_disconnection_done)
        else:
            self.signal_emitter.emit_signal(self.signal_emitter.serial_disconnection_done)

    @synchronize_in_thread
    def send_to_serial(self, information):
        if self.serial is not None:
            if not self.serial.is_open:
                self.serial.open()
            self.serial.write((information + "\n").encode("utf-8"))
            self.serial.reset_input_buffer()
        else:
            self.signal_emitter.emit_signal(self.signal_emitter.serial_is_not_connect)

    def terminate_thread(self):
        self.disconnect()
        self.run_available = False

    def send_g_code(self, g_code: List[str]):
        for string in g_code:
            string = string.strip()
            if string[0] != '#':
                if self.serial is not None:
                    self.send_to_serial(string)
                    self.signal_emitter.emit_signal(self.signal_emitter.sent_g_code_string, string)
                else:
                    return None
