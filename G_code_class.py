from re import match


class G_code_generator:
    def __init__(self, g_code: list or None, max_x_position: float or None, max_y_position: float or None,
                 max_z_position: float or None, x_offset: float or None,
                 y_offset: float or None, z_offset: float or None, standard_fedrate: float):
        self.g_code = g_code
        self.max_x_position = max_x_position
        self.max_y_position = max_y_position
        self.max_z_position = max_z_position
        self.x_offset = x_offset
        self.y_offset = y_offset
        self.z_offset = z_offset
        self.standard_fedrate = standard_fedrate

    def move_to_position(self, prefix: str = '', x: float = None, y: float = None, z: float = None, e: float = None,
                           speed: float = None) -> str:
        g_code_line = prefix + ' '
        g_code_line += 'G1 '
        if x is not None and self.max_x_position is not None:
            if x <= self.max_x_position:
                g_code_line += 'X{} '.format(x)
            else:
                raise ValueError('Value is out of moving range of X axis')
        if y is not None and self.max_y_position is not None:
            if y <= self.max_y_position:
                g_code_line += 'Y{} '.format(y)
            else:
                raise ValueError('Value is out of moving range of Y axis')
        if z is not None and self.max_z_position is not None:
            if z <= self.max_z_position:
                g_code_line += 'Z{} '.format(z)
            else:
                raise ValueError('Value is out of moving range of Z axis')
        if e is not None:
            g_code_line += 'E{} '.format(e)

        if speed is not None:
            g_code_line += 'F{}'.format(speed)
        else:
            g_code_line += 'F{}'.format(self.standard_fedrate)

        self.g_code.append(g_code_line)

        return g_code_line

    def servo_move(self, prefix: str, index: int, servo_lvl: int) -> str:
        # custom g-code for servo move
        # S1 I<index> L<position>
        # <index> - servo index to set
        # <position> - position index(for example: angle)
        g_code_line = prefix + ' '
        g_code_line += 'S1 I{} L{}'.format(index, servo_lvl)

        self.g_code.append(g_code_line)

        return g_code_line

    def solenoid_move(self, prefix: str, index: int, solenoid_lvl: int) -> str:
        # custom g-code for solenoid move
        # S2 I<index> L<position>
        # <index> - servo index to set
        # <position> - position index 1 or 0(open or close)
        if solenoid_lvl not in [0, 1]:
            raise ValueError('Uncorrected solenoid level in S2 G-command.')
        else:
            g_code_line = prefix + ' '
            g_code_line += 'S2 I{} L{}'.format(index, solenoid_lvl)

        self.g_code.append(g_code_line)

        return g_code_line

    def restart_servos_or_solenoids(self, prefix: str or None, type_index: int or None, index: int or None) -> str:
        # custom g-code for solenoid move
        # S0 T<type index> I<index>
        # <type_index> - 0(servo type index), 1(solenoid type index)
        # <index> - solenoid or servo index to set
        # if both parameters is NONE restart all solenoids and servos
        # if only type index parameter restart all set devises
        if prefix is not None:
            g_code_line = prefix + ' '
        else:
            g_code_line = ''
        g_code_line += 'S0'
        if type_index is not None:
            g_code_line += ' T{}'
        if index is not None:
            g_code_line += ' I{}'.format(index)

        self.g_code.append(g_code_line)

        return g_code_line

    def auto_home_axis(self, prefix: str or None, xyze: str = '') -> str:
        if prefix is not None:
            g_code_line = prefix + ' '
        else:
            g_code_line = ''
        g_code_line += 'G28 '
        xyze = xyze.upper()
        xyze = xyze.replace(' ', '')
        if len(xyze) > 3:
            raise ValueError('Incorrect input for G28 G-code command.')
        else:
            if 'X' in xyze:
                g_code_line += 'X '
            if 'Y' in xyze:
                g_code_line += 'Y '
            if 'Z' in xyze:
                g_code_line += 'Z '
            if 'E' in xyze:
                g_code_line += 'E '

        self.g_code.append(g_code_line)
        self.g_code.append("G92 X{} Y{} Z{} E0".format(self.x_offset, self.y_offset, self.z_offset))

        return g_code_line

    def set_position(self, prefix: str or None, x: float = None, y: float = None, z: float = None):
        if x is None and y is None and z is None:
            return 0
        else:
            if prefix is None:
                raise ValueError("__set_position() method need to have prefix value(P<index>)")
            else:
                prefix = self.correct_prefix(prefix)
                g_code_line = prefix + ' G92 '
                if x is not None:
                    g_code_line += 'X{} '.format(x)
                if y is not None:
                    g_code_line += 'Y{} '.format(y)
                if z is not None:
                    g_code_line += 'Z{}'.format(z)

                self.g_code.append(g_code_line)

    def clean(self):
        self.g_code.clear()

    @staticmethod
    def correct_prefix(prefix: str):
        correct_form = r'^P[0-9]$'
        if type(prefix) is str:
            prefix = prefix.upper()
            prefix = prefix.replace(' ', '')
            if match(correct_form, prefix):
                if len(prefix) > 2:
                    raise ValueError('Incorrect prefix input')
                else:
                    return prefix
            else:
                raise ValueError('Incorrect prefix input')
