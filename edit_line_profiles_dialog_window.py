from PyQt5 import QtGui
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QDoubleValidator, QFont
from PyQt5.QtWidgets import QMainWindow, QGridLayout,\
    QWidget, QLabel, QLineEdit, QPushButton, QErrorMessage


from signals_emitter import Signals_emitter
from line_profile_class import Line_profile


class Edit_line_profile_Dialog(QMainWindow):
    __positive_floatValidator = QDoubleValidator(0.0, 100.0, 2)
    __buttons_font = QFont("DejaVu Sans", 14, QFont.Bold)
    __yellow = "background-color: rgb(250, 255, 180);"
    __white = "background-color: rgb(250, 250, 250);"
    new_line_profile_was_created_signal = pyqtSignal(Line_profile)
    edit_line_profile_dialog_close = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.__rewrite_line_profile_flag = False
        self.__editing_line_profile = None
        self.__signal_emitter = Signals_emitter()
        self.setWindowIconText('Line profile edit')

        self.setFixedSize(300, 250)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

        self.grid_widget = QWidget()
        self.grid = QGridLayout(self.grid_widget)
        self.setCentralWidget(self.grid_widget)

        self.add_del_block_grid_widget = QWidget()
        self.add_del_block_grid = QGridLayout(self.add_del_block_grid_widget)

        self.line_profile_name_label = QLabel()
        self.line_profile_name_label.setText('Profile name:')

        self.line_profile_name_line_edit = QLineEdit()
        self.line_profile_name_line_edit.textEdited.connect(self.__set_all_line_edit_white)

        self.line_diameter_label = QLabel()
        self.line_diameter_label.setText('Line diameter:')

        self.line_diameter_line_edit = QLineEdit()
        self.line_diameter_line_edit.setValidator(Edit_line_profile_Dialog.__positive_floatValidator)
        self.line_diameter_line_edit.textEdited.connect(self.__set_all_line_edit_white)

        self.knot_len_label = QLabel()
        self.knot_len_label.setText('Knot length:')

        self.knot_len_line_edit = QLineEdit()
        self.knot_len_line_edit.setValidator(Edit_line_profile_Dialog.__positive_floatValidator)
        self.knot_len_line_edit.textEdited.connect(self.__set_all_line_edit_white)

        self.knot_width_label = QLabel()
        self.knot_width_label.setText('Knot width:')

        self.knot_width_line_edit = QLineEdit()
        self.knot_width_line_edit.setValidator(Edit_line_profile_Dialog.__positive_floatValidator)
        self.knot_width_line_edit.textEdited.connect(self.__set_all_line_edit_white)

        self.need_len_for_one_knot_label = QLabel()
        self.need_len_for_one_knot_label.setText('Need length for knot:')

        self.need_len_for_one_knot_line_edit = QLineEdit()
        self.need_len_for_one_knot_line_edit.setValidator(Edit_line_profile_Dialog.__positive_floatValidator)
        self.need_len_for_one_knot_line_edit.textEdited.connect(self.__set_all_line_edit_white)

        self.ok_button = QPushButton('OK')
        self.ok_button.setFixedSize(100, 50)
        self.ok_button.setFont(Edit_line_profile_Dialog.__buttons_font)

        self.cancel_button = QPushButton('Cancel')
        self.cancel_button.setFixedSize(100, 50)
        self.cancel_button.setFont(Edit_line_profile_Dialog.__buttons_font)
        self.cancel_button.clicked.connect(self.__cancel_button_handler)

        self.grid.addWidget(self.line_profile_name_label, 0, 0, 1, 1)
        self.grid.addWidget(self.line_profile_name_line_edit, 0, 1, 1, 1)

        self.grid.addWidget(self.knot_len_label, 1, 0, 1, 1)
        self.grid.addWidget(self.knot_len_line_edit, 1, 1, 1, 1)
        self.grid.addWidget(self.knot_width_label, 2, 0, 1, 1)
        self.grid.addWidget(self.knot_width_line_edit, 2, 1, 1, 1)

        self.grid.addWidget(self.need_len_for_one_knot_label, 3, 0, 1, 1)
        self.grid.addWidget(self.need_len_for_one_knot_line_edit, 3, 1, 1, 1)
        self.grid.addWidget(self.line_diameter_label, 4, 0, 1, 1)
        self.grid.addWidget(self.line_diameter_line_edit, 4, 1, 1, 1)

        self.add_del_block_grid.addWidget(self.ok_button, 0, 0, 1, 1)
        self.add_del_block_grid.addWidget(self.cancel_button, 0, 1, 1, 1)

        self.grid.addWidget(self.add_del_block_grid_widget, 5, 0, 1, 2)

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        super().close()
        self.__signal_emitter.emit_signal(self.edit_line_profile_dialog_close)

    def rewrite_line_profile(self, line_profile: Line_profile):
        self.__editing_line_profile = line_profile
        self.line_profile_name_line_edit.setText(str(line_profile.profile_name))
        self.knot_len_line_edit.setText(str(line_profile.knot_len))
        self.knot_width_line_edit.setText(str(line_profile.knot_width))
        self.need_len_for_one_knot_line_edit.setText(str(line_profile.need_len_for_one_knot))
        self.line_diameter_line_edit.setText(str(line_profile.line_diameter))
        self.__set_all_line_edit_white()
        self.__reconnect_function_to_button(self.ok_button, self.__ok_button_handler_for_edit_line_profile)
        self.show()

    def add_line_profile(self):
        self.__editing_line_profile = None
        self.line_profile_name_line_edit.clear()
        self.knot_len_line_edit.clear()
        self.knot_width_line_edit.clear()
        self.need_len_for_one_knot_line_edit.clear()
        self.line_diameter_line_edit.clear()
        self.__set_all_line_edit_white()
        self.__reconnect_function_to_button(self.ok_button, self.__ok_button_handler_for_add_line_profile)
        self.show()

    def __ok_button_handler_for_add_line_profile(self):
        need_to_set_flag = False
        if len(self.line_profile_name_line_edit.text().strip()) == 0:
            self.line_profile_name_line_edit.setStyleSheet(Edit_line_profile_Dialog.__yellow)
            need_to_set_flag = True
        else:
            line_profile_name = self.line_profile_name_line_edit.text().strip()
        if len(self.knot_len_line_edit.text().strip()) == 0:
            self.knot_len_line_edit.setStyleSheet(Edit_line_profile_Dialog.__yellow)
            need_to_set_flag = True
        else:
            knot_len = float(self.knot_len_line_edit.text().replace(',', '.'))
        if len(self.knot_width_line_edit.text().strip()) == 0:
            self.knot_width_line_edit.setStyleSheet(Edit_line_profile_Dialog.__yellow)
            need_to_set_flag = True
        else:
            knot_width = float(self.knot_width_line_edit.text().replace(',', '.'))
        if len(self.need_len_for_one_knot_line_edit.text().strip()) == 0:
            self.need_len_for_one_knot_line_edit.setStyleSheet(Edit_line_profile_Dialog.__yellow)
            need_to_set_flag = True
        else:
            need_len_for_one_knot = float(self.need_len_for_one_knot_line_edit.text().replace(',', '.'))
        if len(self.line_diameter_line_edit.text().strip()) == 0:
            self.line_diameter_line_edit.setStyleSheet(Edit_line_profile_Dialog.__yellow)
            need_to_set_flag = True
        else:
            line_diameter = float(self.line_diameter_line_edit.text().replace(',', '.'))

        if need_to_set_flag:
            self.error_message = QErrorMessage()
            self.error_message.setWindowFlags(Qt.WindowStaysOnTopHint)
            self.error_message.setWindowTitle('Not everything is filled')
            self.error_message.showMessage("Fill all parameters.\n\n"
                                           "If you don't want to fill parameter you can paste 0")
        else:
            new_profile = Line_profile(line_profile_name, line_diameter, knot_len, knot_width, need_len_for_one_knot)
            self.__signal_emitter.emit_signal(self.new_line_profile_was_created_signal, new_profile)
            self.__signal_emitter.emit_some_parameter_was_change()
            self.close()

    def __ok_button_handler_for_edit_line_profile(self):
        if len(self.line_profile_name_line_edit.text().strip()) == 0:
            self.__editing_line_profile.profile_name = 'Untiled'
        else:
            self.__editing_line_profile.profile_name = self.line_profile_name_line_edit.text().strip()

        if len(self.knot_len_line_edit.text().strip()) == 0:
            self.__editing_line_profile.knot_len = 0.0
        else:
            self.__editing_line_profile.knot_len = float(self.knot_len_line_edit.text().replace(',', '.'))

        if len(self.knot_width_line_edit.text().strip()) == 0:
            self.__editing_line_profile.knot_width = 0.0
        else:
            self.__editing_line_profile.knot_width = float(self.knot_width_line_edit.text().replace(',', '.'))

        if len(self.need_len_for_one_knot_line_edit.text().strip()) == 0:
            self.__editing_line_profile.need_len_for_one_knot = 0.0
        else:
            self.__editing_line_profile.need_len_for_one_knot = float(self.need_len_for_one_knot_line_edit.text()
                                                                      .replace(',', '.'))

        if len(self.line_diameter_line_edit.text().strip()) == 0:
            self.__editing_line_profile.line_diameter = 0.0
        else:
            self.__editing_line_profile.line_diameter = float(self.line_diameter_line_edit.text().replace(',', '.'))

        self.__signal_emitter.emit_some_parameter_was_change()
        self.close()

    def __cancel_button_handler(self):
        self.close()

    def __set_all_line_edit_white(self):
        self.line_profile_name_line_edit.setStyleSheet(Edit_line_profile_Dialog.__white)
        self.knot_len_line_edit.setStyleSheet(Edit_line_profile_Dialog.__white)
        self.knot_width_line_edit.setStyleSheet(Edit_line_profile_Dialog.__white)
        self.need_len_for_one_knot_line_edit.setStyleSheet(Edit_line_profile_Dialog.__white)
        self.line_diameter_line_edit.setStyleSheet(Edit_line_profile_Dialog.__white)

    @staticmethod
    def __reconnect_function_to_button(button: QPushButton, function):
        try:
            button.disconnect()
        except:
            pass
        button.clicked.connect(function)