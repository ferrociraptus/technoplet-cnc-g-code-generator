from PyQt5 import QtWidgets, QtGui
from point_class import Point
from image_rotate_class import ImageRotate
from color_class import Color


class Knot(QtWidgets.QPushButton):

    def knot_init(self, x, y, fabric_widget):
        self.position = Point(x, y)
        self.color = Color('#ffffff')

        self.fabric = fabric_widget

        self.image_rotate = ImageRotate(self)
        self.setFixedSize(50, 50)
        self.update_image()
        self.setMask(QtGui.QRegion(self.rect(), QtGui.QRegion.Ellipse))
        self.clicked.connect(self.__change_image_after_click)
        self.setToolTip('x:{}; y:{}'.format(self.position.x, self.position.y))

    def change_color(self, red=None, green=None, blue=None):
        self.color.change_color(red, green, blue)
        self.update_image()

    def update_image(self):
        # Change knot icon
        self.setStyleSheet(("border-image: url({}); background-color: {};")
                           .format(self.image_rotate.image(), self.color.color_in_hex('#')))

    def __change_image_after_click(self):
        # Handler for knot button
        self.image_rotate.next_image()
        self.update_image()

        self.fabric.grid_update()
